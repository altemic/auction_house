package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Auction;

/**
 * Created by michal on 13.11.2016.
 */
public interface AuctionRepository extends JpaRepository<Auction, Long> {
}

package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Role;

/**
 * Created by michal on 13.11.2016.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
}

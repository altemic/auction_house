package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.PaymentStatus;

/**
 * Created by michal on 13.11.2016.
 */
public interface PaymentStatusRepository extends JpaRepository<PaymentStatus, Long> {
}

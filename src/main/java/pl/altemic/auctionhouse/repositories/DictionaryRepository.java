package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Dictionary;

/**
 * Created by michal on 13.11.2016.
 */
public interface DictionaryRepository extends JpaRepository<Dictionary, Long> {
    Dictionary findBySymbol(String symbol);
}

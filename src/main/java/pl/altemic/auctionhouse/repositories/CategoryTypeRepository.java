package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.CategoryType;

/**
 * Created by m.altenberg on 20.12.2016.
 */
public interface CategoryTypeRepository extends JpaRepository<CategoryType, Long> {
    CategoryType findByName(String name);
}

package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Description;

/**
 * Created by michal on 13.11.2016.
 */
public interface DescriptionRepository extends JpaRepository<Description, Long> {
}

package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.BillingData;

/**
 * Created by michal on 13.11.2016.
 */
public interface BillingDataRepository extends JpaRepository<BillingData, Long> {
}

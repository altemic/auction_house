package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.User;

/**
 * Created by michal on 13.11.2016.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}

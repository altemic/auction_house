package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Cart;

/**
 * Created by michal on 13.11.2016.
 */
public interface CartRepository extends JpaRepository<Cart, Long> {
}

package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Payment;

/**
 * Created by michal on 13.11.2016.
 */
public interface PaymentRepository extends JpaRepository<Payment, Long> {
}

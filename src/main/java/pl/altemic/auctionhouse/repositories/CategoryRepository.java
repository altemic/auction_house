package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Category;

/**
 * Created by m.altenberg on 20.12.2016.
 */
public interface CategoryRepository extends JpaRepository<Category,Long> {
    Category findByName(String name);
}

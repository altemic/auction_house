package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Shipping;

/**
 * Created by michal on 13.11.2016.
 */
public interface ShippingRepository extends JpaRepository<Shipping, Long> {
}

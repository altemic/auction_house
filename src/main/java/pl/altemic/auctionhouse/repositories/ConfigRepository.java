package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Config;

/**
 * Created by michal on 20.11.2016.
 */
public interface ConfigRepository extends JpaRepository<Config, Long> {
    Config findByName(String name);
}

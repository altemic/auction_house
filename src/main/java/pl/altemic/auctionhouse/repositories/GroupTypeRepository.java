package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.GroupType;

/**
 * Created by michal on 13.11.2016.
 */
public interface GroupTypeRepository extends JpaRepository<GroupType, Long> {
    GroupType findByName(String name);
}

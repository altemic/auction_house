package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Permission;

/**
 * Created by michal on 13.11.2016.
 */
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    Permission findByNameAndReference(String name, String reference);
    Permission findByName(String name);
    Permission findByTypeAndReference(Integer type, String reference);
}

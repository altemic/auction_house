package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Title;

import java.util.List;

/**
 * Created by michal on 19.11.2016.
 */
public interface TitleRepository extends JpaRepository<Title, Long> {
    List<Title> findByText(String text);
    List<Title> findByOwner(long id);
}

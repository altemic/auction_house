package pl.altemic.auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.altemic.auctionhouse.models.Group;

/**
 * Created by michal on 13.11.2016.
 */
public interface GroupRepository extends JpaRepository<Group, Long> {
    Group findByName(String name);
}

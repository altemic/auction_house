package pl.altemic.auctionhouse.viewmodels;

import org.springframework.beans.factory.annotation.Autowired;
import pl.altemic.auctionhouse.dto.Auction;
import pl.altemic.auctionhouse.utils.Logger;

import java.util.stream.Collectors;

/**
 * Created by m.altenberg on 03.03.2017.
 */
//During adding there is no need for information about seller, but during viewing it is
public class AuctionViewModel {
    private Auction dto;
    String title;
    String description;
    String dictSymbol;
    Logger logger;

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public AuctionViewModel(){}

    public AuctionViewModel(Auction dto, String dict){
        if(translates(dto, dict)){
            this.title = dto.getTitleList().stream().
                    filter(t -> t.getDictionary().getSymbol().equals(dict)).
                    collect(Collectors.toList()).get(0).getText();
            this.description = dto.getDescriptionList().stream().
                    filter(d -> d.getDictionary().getSymbol().equals(dict)).
                    collect(Collectors.toList()).get(0).getText();
            this.dictSymbol = dict;
            this.dto = dto;
        }
    }

    private boolean translates(Auction dto, String dict) {
        try {
            boolean title = dto.getTitleList().stream().anyMatch(t -> t.getDictionary().getSymbol().equals(dict));
            boolean description = dto.getDescriptionList().stream().anyMatch(d -> d.getDictionary().getSymbol().equals(dict));
            return title && description;
        } catch (Exception e){
            logger.error("Error checking dto.Auction: \n" + e);
            return false;
        }
    }

    public boolean isReady(){
        if(this.dto!=null && this.dictSymbol!="") {
            return translates(dto, dictSymbol);
        } else if(dto == null){
            return title != "" && description != "" && dictSymbol != "";
        }
        return false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDictSymbol() {
        return dictSymbol;
    }

    public void setDictSymbol(String dictSymbol) {
        this.dictSymbol = dictSymbol;
    }
}

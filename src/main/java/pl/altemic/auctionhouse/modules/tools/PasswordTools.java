package pl.altemic.auctionhouse.modules.tools;

import org.springframework.stereotype.Component;

/**
 * Created by michal on 26.02.2017.
 */

@Component
public class PasswordTools {
    public String encrypt(String text){
        return text;
    }

    public String decrypt(String text){
        return text;
    }

    public boolean matchesRequirements(String password){
        return true;
    }
}

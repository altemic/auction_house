package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.Group;
import pl.altemic.auctionhouse.services.GroupService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

import javax.annotation.PostConstruct;

/**
 * Created by michal on 19.11.2016.
 */

@Service
public class GroupModule extends SimpleModule<Group> implements Module<Group> {

    GroupService groupService;

    @Autowired
    public void setGroupService(@Lazy GroupService groupService) {
        this.groupService = groupService;
    }

    @PostConstruct
    public void initialize(){
        super.setCrudService(groupService);
        super.setEx(message -> {
            throw new GroupException(message);
        });
    }

    public Group findByName(String name){
        return groupService.findByName(name);
    }

    public class GroupException extends AuctionHouseException{
        public GroupException(String message){
            super(message);
        }
    }
}

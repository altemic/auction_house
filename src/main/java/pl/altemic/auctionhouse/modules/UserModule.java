package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.Group;
import pl.altemic.auctionhouse.dto.Role;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.services.RoleService;
import pl.altemic.auctionhouse.services.UserService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;
import pl.altemic.auctionhouse.utils.Mapper;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

/**
 * Created by michal on 19.11.2016.
 */

@Service
public class UserModule extends SimpleModule<User> implements Module<User> {

    UserService userService;
    RoleService roleService;
    Mapper<User, User> mapper;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public UserModule(Mapper<User, User> _mapper){
        this.mapper = _mapper;
    }

    public void add(String _username, String _firstName, String _lastname, Group _group) {
        User user = new User();
        user.setUsername(_username);
        user.setFirstname(_firstName);
        user.setLastname(_lastname);
        user.setGroup(_group);
        userService.save(user);
    }

    public void addCustomRoleToUser(User _user, Role _role) {
        if(_user.getCustomRoleList() == null){
            _user.setCustomRoleList(new ArrayList<>());
        }
        _user.getCustomRoleList().add(_role);
        userService.update(_user);
    }

    public User findByUsername(String username){
        if(username.equals("")) return null;
        User user = userService.findByUsername(username);
        return user;
    }

    @Override
    @PostConstruct
    public void initialize() {
        super.setCrudService(userService);
        super.setEx(message -> {
            throw new UserException(message);
        });
    }

    public VerificationResponse<User> verify(User dto){
        VerificationResponse<User> vr = new VerificationResponse<>(dto);
        if(dto == null){
            vr.setCorrect(false);
        }
        if(dto.getUsername() == null || dto.getUsername().equals("")){
            vr.setCorrect(false);
        }
        return vr;
    }

    public class UserException extends AuctionHouseException{
        public UserException(String message){
            super(message);
        }
    }
}

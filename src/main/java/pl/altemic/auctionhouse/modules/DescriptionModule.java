package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.Description;
import pl.altemic.auctionhouse.services.DescriptionService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

import javax.annotation.PostConstruct;

/**
 * Created by m.altenberg on 26.02.2017.
 */
@Service
public class DescriptionModule extends SimpleModule<Description> implements Module<Description> {

    DescriptionService descriptionService;

    @Autowired
    public void setDescriptionService(DescriptionService descriptionService) {
        this.descriptionService = descriptionService;
    }

    @Override
    @PostConstruct
    public void initialize() {
        super.setCrudService(descriptionService);
        super.setEx(message -> {
            throw new DescriptionException(message);
        });
    }

    public class DescriptionException extends AuctionHouseException{
        public DescriptionException(String message){
            super(message);
        }
    }
}

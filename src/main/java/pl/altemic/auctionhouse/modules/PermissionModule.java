package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.Permission;
import pl.altemic.auctionhouse.dto.PermissionType;
import pl.altemic.auctionhouse.dto.Role;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.services.PermissionService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 19.11.2016.
 */

@Service
public class PermissionModule extends SimpleModule<Permission> implements Module<Permission> {

    private PermissionService permissionService;
    private RoleModule roleModule;

    @Autowired
    public void setRoleModule(RoleModule roleModule) {
        this.roleModule = roleModule;
    }

    @Autowired
    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @PostConstruct
    public void initialize(){
        super.setCrudService(permissionService);
        super.setEx(message -> {
            throw new PermissionException(message);
        });
    }

    public void add(String name, PermissionType type, Class c){
        String className = c.getSimpleName();
        Permission permission = new Permission(name, type, className);
        permissionService.save(permission);
    }

    public Permission findByNameAndReference(String name, String reference){
        return permissionService.findByNameAndReference(name, reference);
    }

    public Permission findByTypeAndReference(PermissionType permissionType, String reference){
        return findByTypeAndReference(permissionType.toString(), reference);
    }

    public Permission findByTypeAndReference(String permissionType, String reference){
        Permission permission = permissionService.findByTypeAndReference(permissionType, reference);
        return permission;
    }

    public Permission findByName(String name){
        return permissionService.findByName(name);
    }

    public List<Permission> getPermissionsForUser(User user){
        List<Role> userRoleList = roleModule.getRolesForUser(user);
        List<Permission> userPermissionList = new ArrayList<>();
        for(Role r : userRoleList){
            if(r.getPermissionList()!=null){
                userPermissionList.addAll(r.getPermissionList());
            }
        }
        return userPermissionList;
    }

    public class PermissionException extends AuctionHouseException{
        public PermissionException(String message){
            super(message);
        }
    }
}

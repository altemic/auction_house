package pl.altemic.auctionhouse.modules;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by m.altenberg on 04.02.2017.
 */
public class VerificationResponse<T> {
    T verifiedObject;
    boolean correct;
    List<String> messages;

    public VerificationResponse(T _verifiedObject){
        this.verifiedObject = _verifiedObject;
        correct = true;
        messages = new ArrayList<>(10);
    }

    public boolean isCorrect(){
        return correct;
    }

    public void setCorrect(boolean _correct){
        this.correct = _correct;
    }

    public T getVerifiedObject() {
        return verifiedObject;
    }

    public void addMessage(String messageLine){
        messages.add(messageLine);
    }

    public List<String> getMessages(){
        return messages;
    }

    public String getFormattedMessage(){
        String message = messages.stream().map(s -> s).collect(Collectors.joining("\n"));
        return message;
    }
}

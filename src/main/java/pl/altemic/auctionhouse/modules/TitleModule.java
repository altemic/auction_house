package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.Title;
import pl.altemic.auctionhouse.services.TitleService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

import javax.annotation.PostConstruct;

/**
 * Created by m.altenberg on 15.02.2017.
 */

@Service
public class TitleModule extends SimpleModule<Title> implements Module<Title> {
    TitleService titleService;
    DictionaryModule dictionaryModule;

    @Autowired
    public void setTitleService(TitleService titleService) {
        this.titleService = titleService;
    }

    @Autowired
    public void setDictionaryModule(DictionaryModule dictionaryModule) {
        this.dictionaryModule = dictionaryModule;
    }

    public TitleModule(){}

    @Override
    @PostConstruct
    public void initialize(){
        super.setCrudService(titleService);
        super.setEx(message -> {
            throw new TitleException(message);
        });
    }

    @Override
    public VerificationResponse<Title> verify(Title obj) {
        VerificationResponse<Title> vr = new VerificationResponse<>(obj);
        if(obj.getOwner() == null){
            vr.setCorrect(false);
        }
        if(vr.isCorrect() && (obj.getText() == null || obj.getText().equals(""))){
            vr.setCorrect(false);
        }
        if(vr.isCorrect() && (obj.getDictionary() == null || obj.getDictionary().equals("") || dictionaryModule.dictionaryExists(obj.getDictionary().getSymbol()))){
            vr.setCorrect(false);
        }
        vr.setCorrect(true);
        return vr;
    }

    public class TitleException extends AuctionHouseException{
        public TitleException(String message){
            super(message);
        }
    }
}

package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.GroupType;
import pl.altemic.auctionhouse.dto.Role;
import pl.altemic.auctionhouse.services.GroupTypeService;
import pl.altemic.auctionhouse.services.RoleService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

import javax.annotation.PostConstruct;

/**
 * Created by michal on 19.11.2016.
 */

@Service
public class GroupTypeModule extends SimpleModule<GroupType> implements Module<GroupType> {

    GroupTypeService groupTypeService;
    RoleService roleService;

    @Autowired
    public void setGroupTypeService(GroupTypeService groupTypeService) {
        this.groupTypeService = groupTypeService;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }


    public void addRoleToGroupType(GroupType _groupType, Role _role){
        _groupType.getRoleList().add(_role);
        groupTypeService.update(_groupType);
    }

    public GroupType findByName(String name){
        return groupTypeService.findByName(name);
    }

    @Override
    @PostConstruct
    public void initialize() {
        setCrudService(groupTypeService);
        setEx(message -> {
            throw new GroupTypeException(message);
        });
    }

    public class GroupTypeException extends AuctionHouseException{
        public GroupTypeException(String message){
            super(message);
        }
    }
}

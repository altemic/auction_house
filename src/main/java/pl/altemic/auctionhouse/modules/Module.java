package pl.altemic.auctionhouse.modules;

import pl.altemic.auctionhouse.dto.BaseDto;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

/**
 * Created by m.altenberg on 15.02.2017.
 */
public interface Module<T extends BaseDto> {
    void remove(T obj) throws AuctionHouseException;
    T update(T obj) throws AuctionHouseException;
    T add(T obj)  throws AuctionHouseException;
    T findOne(long id) throws AuctionHouseException;
    VerificationResponse<T> verify(T obj);
}

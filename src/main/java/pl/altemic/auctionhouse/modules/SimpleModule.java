package pl.altemic.auctionhouse.modules;

import org.springframework.transaction.annotation.Transactional;
import pl.altemic.auctionhouse.dto.BaseDto;
import pl.altemic.auctionhouse.services.BaseCRUDService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;
import pl.altemic.auctionhouse.utils.AuctionHouseExceptionThrower;

/**
 * Created by m.altenberg on 21.02.2017.
 */
@Transactional
public abstract class SimpleModule<T extends BaseDto> implements Module<T> {

    BaseCRUDService<T> crudService;
    AuctionHouseExceptionThrower ex;

    public SimpleModule(BaseCRUDService<T> _crudService, AuctionHouseExceptionThrower _exceptionThrower){
        this.crudService = _crudService;
        this.ex = _exceptionThrower;
    }

    public SimpleModule(){}

    public abstract void initialize();

    public void setCrudService(BaseCRUDService<T> crudService) {
        this.crudService = crudService;
    }

    public void setEx(AuctionHouseExceptionThrower ex) {
        this.ex = ex;
    }

    @Override
    public void remove(T obj) throws AuctionHouseException {
        initialisationVerificator(this);
        if(obj == null) return;
        VerificationResponse vr = verify(obj);
        if(obj.getId() == 0){
            vr.addMessage("ID CANNOT BE 0 IN REMOVE OPERATION");
            vr.setCorrect(false);
        }
        if(!vr.isCorrect()){
            ex.throwException(vr.getFormattedMessage());
        }
        obj.setDeleted(true);
        obj = crudService.update(obj);
    }

    @Override
    public T update(T obj) throws AuctionHouseException {
        initialisationVerificator(this);
        if(obj == null) return null;
        VerificationResponse vr = verify(obj);
        if(!vr.isCorrect() || obj.getId() == 0){
            vr.addMessage("ID CANNOT BE 0 IN UPDATE OPERATION");
            vr.setCorrect(false);
        }
        if(!vr.isCorrect()){
            ex.throwException(vr.getFormattedMessage());
        }
        obj = crudService.update(obj);
        return obj;
    }

    @Override
    public T add(T obj) throws AuctionHouseException {
        initialisationVerificator(this);
        if(obj == null) return null;
        VerificationResponse vr = verify(obj);
        if(!vr.isCorrect()){
            ex.throwException(vr.getFormattedMessage());
        }
        obj = crudService.save(obj);
        return obj;
    }

    @Override
    public T findOne(long id) throws AuctionHouseException {
        initialisationVerificator(this);
        if(id == 0) return null;
        T obj = crudService.findOne(id);
        return obj;
    }

    @Override
    public VerificationResponse verify(T obj){
        VerificationResponse<T> vr = new VerificationResponse<T>(obj);
        if(obj == null){
            vr.setCorrect(false);
        }
        vr.setCorrect(true);
        return vr;
    }

    private void initialisationVerificator(SimpleModule<T> simpleModule) throws AuctionHouseException {
        if(simpleModule.crudService == null){
            throw new AuctionHouseException("CRUD SERVICE HASN'T BEEN INITIALISED IN SIMPLE MODULE");
        }
        if(simpleModule.ex == null){
            throw new AuctionHouseException("EXCEPTION THROWER HASN'T BEEN INITIALISED IN SIMPLE MODULE");
        }
    }

}

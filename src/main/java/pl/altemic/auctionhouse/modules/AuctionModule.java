package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.Auction;
import pl.altemic.auctionhouse.services.AuctionService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

import javax.annotation.PostConstruct;

/**
 * Created by m.altenberg on 01.02.2017.
 */

@Service
public class AuctionModule extends SimpleModule<Auction> implements Module<Auction> {
    AuctionService auctionService;

    @Autowired
    public void setAuctionService(AuctionService auctionService) {
        this.auctionService = auctionService;
    }

    @Override
    @PostConstruct
    public void initialize(){
        super.setCrudService(auctionService);
        super.setEx(message -> {
            throw new AuctionException(message);
        });
    }

    public VerificationResponse<Auction> verify(Auction dto){
        if(dto == null) return null;
        VerificationResponse<Auction> vr = new VerificationResponse<>(dto);
        if(dto.getTitleList() == null || dto.getTitleList().size() == 0){
            vr.setCorrect(false);
        }
        if(dto.getDescriptionList() == null || dto.getDescriptionList().size() == 0){
            vr.setCorrect(false);
        }
        if(dto.getSeller() == null){
            vr.setCorrect(false);
        }
        vr.setCorrect(true);
        return vr;
    }

    public class AuctionException extends AuctionHouseException{
        public AuctionException(String message){
            super(message);
        }
    }
}

package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import pl.altemic.auctionhouse.dto.Config;
import pl.altemic.auctionhouse.services.ConfigService;
import pl.altemic.auctionhouse.utils.Settings;

import java.lang.reflect.Field;

/**
 * Created by michal on 20.11.2016.
 */

@Component
public class ConfigModuleImpl implements ConfigModule {

    ConfigService configService;

    @Autowired
    public void setConfigService(@Lazy  ConfigService configService) {
        this.configService = configService;
    }

    public Settings load(){
        Settings settings = new Settings();
        Field[] fields = settings.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            String name = f.getName();
            Config cfgFromDb = configService.findByName(name);
            String valueFromDb = "";
            if(cfgFromDb!=null){
                valueFromDb = cfgFromDb.getValue();
                try {
                    if (valueFromDb != null) {
                        f.set(settings, valueFromDb);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return settings;
    }

    public void save(Settings settings) {
        Field[] fields = settings.getClass().getDeclaredFields();
        for(Field f : fields){
            f.setAccessible(true);
            try {
                String name = f.getName();
                String value = f.get(settings).toString();
                Config config = new Config();
                config.setName(name);
                config.setValue(value);
                configService.update(config);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}

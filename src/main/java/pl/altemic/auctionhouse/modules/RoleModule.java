package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.altemic.auctionhouse.dto.GroupType;
import pl.altemic.auctionhouse.dto.Role;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.services.RoleService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 19.11.2016.
 */

@Service
@Transactional
public class RoleModule extends SimpleModule<Role> implements Module<Role> {

    private RoleService roleService;

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    @PostConstruct
    public void initialize(){
        super.setCrudService(roleService);
        super.setEx(message -> {
            throw new RoleException(message);
        });
    }

    public Role findByName(String name) { return roleService.findByName(name); }

    public List<Role> getRolesForUser(User user){
        List<Role> roleList = new ArrayList<>();
        if(user.getCustomRoleList()!=null) {
            roleList.addAll(user.getCustomRoleList());
        }
        if(user.getGroup()!= null && user.getGroup().getGroupTypes()!=null && user.getGroup().getGroupTypes().size() > 0){
            for(GroupType gt : user.getGroup().getGroupTypes()){
                roleList.addAll(gt.getRoleList());
            }
        }
        return roleList;
    }

    public class RoleException extends AuctionHouseException{
        public RoleException(String message){
            super(message);
        }
    }
}

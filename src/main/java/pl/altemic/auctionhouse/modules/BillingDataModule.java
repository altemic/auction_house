package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.BillingData;
import pl.altemic.auctionhouse.services.BillingDataService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

import javax.annotation.PostConstruct;

/**
 * Created by m.altenberg on 04.02.2017.
 */
@Service
public class BillingDataModule extends SimpleModule<BillingData> implements Module<BillingData> {

    BillingDataService billingDataService;

    @Autowired
    public void setBillingDataService(BillingDataService billingDataService) {
        this.billingDataService = billingDataService;
    }

    @Override
    @PostConstruct
    public void initialize(){
        super.setCrudService(billingDataService);
        super.setEx(message -> {
            throw new BillingDataException(message);
        });
    }

    public class BillingDataException extends AuctionHouseException{
        public BillingDataException(String message){
            super(message);
        }
    }
}

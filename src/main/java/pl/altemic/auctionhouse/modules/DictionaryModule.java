package pl.altemic.auctionhouse.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.Dictionary;
import pl.altemic.auctionhouse.services.DictionaryService;
import pl.altemic.auctionhouse.utils.AuctionHouseException;

import javax.annotation.PostConstruct;

/**
 * Created by m.altenberg on 15.02.2017.
 */
@Service
public class DictionaryModule extends SimpleModule<Dictionary> implements Module<Dictionary> {

    public static final String POLISH_DICT = "PL";

    DictionaryService dictionaryService;

    @Autowired
    public void setDictionaryService(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @PostConstruct
    public void initialize(){
        super.setCrudService(dictionaryService);
        super.setEx(message -> {
            throw new DictionaryException(message);
        });
    }

    public Dictionary findBySymbol(String symbol){
        Dictionary dictionary = dictionaryService.findBySymbol(symbol);
        return dictionary;
    }

    public boolean dictionaryExists(Dictionary dict){
        return dictionaryExists(dict.getSymbol());
    }

    public boolean dictionaryExists(String symbol){
        Dictionary dictionary = findBySymbol(symbol);
        return dictionary == null;
    }

    @Override
    public VerificationResponse<Dictionary> verify(Dictionary obj) {
        VerificationResponse<Dictionary> vr = new VerificationResponse<>(obj);
        if(obj.getSymbol() == null || obj.getSymbol().equals("")){
            vr.setCorrect(false);
        }
        return vr;
    }

    public class DictionaryException extends AuctionHouseException{
        public DictionaryException(String message){
            super(message);
        }
    }
}

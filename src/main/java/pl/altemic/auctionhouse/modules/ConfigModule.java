package pl.altemic.auctionhouse.modules;

import pl.altemic.auctionhouse.utils.Settings;

/**
 * Created by michal on 20.11.2016.
 */
public interface ConfigModule {
    Settings load();
    void save(Settings settings);
}

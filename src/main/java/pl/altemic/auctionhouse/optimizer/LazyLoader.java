package pl.altemic.auctionhouse.optimizer;

import com.fasterxml.jackson.databind.util.ClassUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import pl.altemic.auctionhouse.dto.BaseDto;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.guardian.GuardianAspect;
import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.services.ServiceFactory;
import pl.altemic.auctionhouse.utils.Common;
import pl.altemic.auctionhouse.utils.DbInitializerImpl;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by m.altenberg on 23.12.2016.
 */

@Aspect
@Component
public class LazyLoader{

    ServiceFactory serviceFactory;

    @Autowired
    public void setServiceFactory(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }


    @Around("within(pl.altemic.auctionhouse.services.*)")
    public Object beforeAdvice(ProceedingJoinPoint pjp) throws Throwable {

        try {

            MethodSignature signature = (MethodSignature) pjp.getSignature();
            Method method = signature.getMethod();

            //Object.class can happen when using T save method -> then  type S is erased after compilation
            if ((!BaseDto.class.isAssignableFrom(method.getReturnType()) && !method.getReturnType().equals(Object.class)) || DbInitializerImpl.firstRun || !(pjp.getTarget() instanceof Optimizer)) {
                Object result = pjp.proceed();
                return result;
            }

            try {
                Object result = pjp.proceed();
                if(result == null) return null;

                ProxyFactory factory = new ProxyFactory(result);
                factory.setProxyTargetClass(true);
                Object dto = factory.getProxy();

                BeforeGetterAdvice advice = new BeforeGetterAdvice();
                advice.setOptimizerAdvice(advice);
                advice.setProxyFactory(factory);
                advice.setServiceFactory(serviceFactory);
                Advisor advisor = new DefaultPointcutAdvisor(advice);
                factory.addAdvisor(advisor);

                return dto;
            } catch (Exception e){
                e.printStackTrace();
            }

            Object result = pjp.proceed();
            return result;
        } catch (Exception e){
            return null;
        } finally {

        }
    }
}

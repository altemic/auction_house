package pl.altemic.auctionhouse.optimizer;

import org.springframework.aop.framework.Advised;

import java.util.Objects;

/**
 * Created by m.altenberg on 29.12.2016.
 */
public class LazyRemover {
    public static <T> T extract(Object target) {
        Advised a = (Advised)target;
        Object retrieved = null;
        try {
            retrieved = a.getTargetSource().getTarget();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T)retrieved;
    }
}

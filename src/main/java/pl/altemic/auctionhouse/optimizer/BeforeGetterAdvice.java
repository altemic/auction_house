package pl.altemic.auctionhouse.optimizer;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.ProxyFactory;
import org.aopalliance.aop.Advice;
import org.springframework.aop.MethodBeforeAdvice;
import pl.altemic.auctionhouse.services.ServiceFactory;
import pl.altemic.auctionhouse.utils.Common;

import java.lang.reflect.*;

/**
 * Created by m.altenberg on 26.12.2016.
 */

public class BeforeGetterAdvice implements MethodBeforeAdvice {

    ServiceFactory serviceFactory;
    Advice optimizerAdvice;
    ProxyFactory proxyFactory;

    public void setServiceFactory(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    public void setOptimizerAdvice(Advice optimizerAdvice) {
        this.optimizerAdvice = optimizerAdvice;
    }

    public void setProxyFactory(ProxyFactory proxyFactory) {
        this.proxyFactory = proxyFactory;
    }

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        if(target == null) return;
        if(!method.getName().toLowerCase().startsWith("get") && Common.isPrimitive(method.getReturnType())){
            return;
        }
        try {
            //TODO: implement full lazy loading - currently basic fields are loaded or all complex
            Optimizer optimizer = serviceFactory.getService(target.getClass().getSimpleName());
            target = optimizer.LoadAllFields(target);
            if(target == null) return;
            if(proxyFactory!= null && optimizerAdvice!=null && proxyFactory.indexOf(optimizerAdvice) > -1){
                proxyFactory.removeAdvice(optimizerAdvice);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
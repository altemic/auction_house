package pl.altemic.auctionhouse.guardian;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import pl.altemic.auctionhouse.services.ServiceFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

/**
 * Created by m.altenberg on 28.01.2017.
 * aim is to find matching entity which key is defined in annotation
 */
@Component
public class GuardianFinderBasicImpl implements GuardianFinder{

    ServiceFactory serviceFactory;

    @Autowired
    public void setServiceFactory(@Lazy ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Override
    public String getFinderName() {
        return this.getClass().getCanonicalName();
    }

    @Override
    public Object find(Class dto, String[] params) {
        try {
            ArrayList<Matcher> fieldMatchers = matcherConverter(params);
            if(fieldMatchers.size() <= 1 && fieldMatchers.get(0).getValue().equals("-1")) return null;
            Object service = serviceFactory.getService(dto.getSimpleName());

            List<Method> finderMethods = Arrays.asList(AopUtils.getTargetClass(service).getDeclaredMethods()).stream().filter(
                    m -> m.getAnnotation(GuardianConfig.class)!= null && !m.getAnnotation(GuardianConfig.class).finderParameter().equals("")
            ).collect(toList());

            List<MethodMatcher> methodMatchers = finderMethods.stream().map(m -> {
                String[] methodParams = m.getAnnotation(GuardianConfig.class).finderParameter();
                List<Matcher> currentMethodMatchers = matcherConverter(methodParams);
                MethodMatcher mm = new MethodMatcher();
                mm.setMethod(m);
                currentMethodMatchers.stream().forEach(cmm -> mm.addMatcher(cmm));
                return mm;
            }).collect(toList());

            List<String> fieldKeys = fieldMatchers.stream().map(x -> x.getKey()).collect(toList());
            //There must be only one matching method in this implementation
            MethodMatcher chosenMethodMatcher = methodMatchers.stream().filter(m -> m.matches(fieldKeys)).collect(toList()).get(0);
            Method theRightOne = chosenMethodMatcher.getMethod();
            Parameter[] parameters = theRightOne.getParameters();

            List args = new ArrayList();
            for(Parameter p : parameters){
                String fieldName = chosenMethodMatcher.getMatchers().stream().filter(m -> m.getValue().equals(p.getName())).collect(toList()).get(0).getKey();
                String value = fieldMatchers.stream().filter(f -> f.getKey().equals(fieldName)).collect(toList()).get(0).getValue();
                args.add(value);
            }

            Object result = theRightOne.invoke(service, args.toArray());
            return result;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Matcher> matcherConverter(String[] input){
        ArrayList<Matcher> matchers = new ArrayList<>();
        for (String qualifier : input){
            String[] param = qualifier.split(":");
            if(param.length != 2) continue;
            Matcher matcher = new Matcher();
            matcher.setKey(param[0]);
            matcher.setValue(param[1]);
            matchers.add(matcher);
        }
        return matchers;
    }

    class MethodMatcher{
        List<Matcher> matchers;
        Method method;

        public MethodMatcher(){
            matchers = new ArrayList<>();
        }

        public void setMethod(Method m){
            this.method = m;
        }

        public Method getMethod(){
            return method;
        }

        public void addMatcher(Matcher m){
            matchers.add(m);
        }

        public List<Matcher> getMatchers(){
            return matchers;
        }

        public int count(){
            return matchers.size();
        }

        public boolean matches(List<String> fieldKeys){
            if(matchers.size()!=fieldKeys.size()) return false;
            for(Matcher matcher : matchers){
                String key = matcher.getKey();
                if(!fieldKeys.stream().anyMatch(o -> o.equals(key))) return false;
            }
            return true;
        }
    }

    class Matcher {
        String key;
        String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}

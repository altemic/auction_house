package pl.altemic.auctionhouse.guardian;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

/**
 * Created by m.altenberg on 09.12.2016.
 */

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PermissionType {
    pl.altemic.auctionhouse.dto.PermissionType permissionType() default pl.altemic.auctionhouse.dto.PermissionType.VIEW;
}

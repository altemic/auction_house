package pl.altemic.auctionhouse.guardian;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.bytecode.buildtime.spi.ExecutionException;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import pl.altemic.auctionhouse.dto.Group;
import pl.altemic.auctionhouse.dto.Permission;
import pl.altemic.auctionhouse.dto.PermissionType;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.modules.PermissionModule;
import pl.altemic.auctionhouse.security.ContextUser;
import pl.altemic.auctionhouse.services.BaseCRUDService;
import pl.altemic.auctionhouse.services.BaseCRUDServiceImpl;
import pl.altemic.auctionhouse.services.ServiceFactory;
import pl.altemic.auctionhouse.utils.Common;
import pl.altemic.auctionhouse.utils.Constants;
import pl.altemic.auctionhouse.utils.DbInitializerImpl;

import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 24.11.2016.
 */

@Aspect
@Component
public class GuardianAspect {

    ServiceFactory serviceFactory;
    ContextUser contextUser;
    PermissionModule permissionModule;
    GuardianFinderFactory guardianFinderFactory;

    @Autowired
    public void setPermissionModule(PermissionModule permissionModule) {
        this.permissionModule = permissionModule;
    }

    @Autowired
    public void setServiceFactory(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Autowired
    public void setContextUser(ContextUser contextUser) {
        this.contextUser = contextUser;
    }

    @Autowired
    public void setGuardianFinderFactory(GuardianFinderFactory guardianFinderFactory) {
        this.guardianFinderFactory = guardianFinderFactory;
    }

    @Around("this(pl.altemic.auctionhouse.services.BaseCRUDService)")
    public Object validatePermissions(ProceedingJoinPoint pjp) throws Throwable {

        try {

            if(DbInitializerImpl.firstRun
                    || !isControlledByGuardian(pjp)
                    || securityOnStackTrace()){ //verify if needed
                Object result = pjp.proceed();
                return result;
            }

            if(getDtoPermissiontype(pjp) == PermissionType.VIEW){
                Object result = pjp.proceed();
                return result;
            }

            Permission permission = getPermission(pjp, null);
            User loggedInUser = contextUser.getLoggedInUser();

            if(getOwner(pjp) == null || userIsOwnerPjp(pjp, loggedInUser) || userHasPermission(permission, loggedInUser)){

                Object arg = pjp.getArgs()[0];
                //to TEST
                if(Ownable.class.isAssignableFrom(arg.getClass())
                        && getDtoPermissiontype(pjp) == PermissionType.ADD
                        && !loggedInUser.getUsername().equals(Constants.ANONUMOUS_USERNAME)){
                    Ownable ownable = (Ownable)arg;
                    if(ownable.getOwner()!=null) {
                        if (User.class.isAssignableFrom(ownable.getOwner().getClass())) {
                            ownable.setOwner(loggedInUser);
                        } else if (Group.class.isAssignableFrom(ownable.getOwner().getClass())) {
                            if (loggedInUser.getGroup() != null) {
                                ownable.setOwner(loggedInUser.getGroup());
                            }
                        }
                    }
                }

                Class dto = getDto(pjp);
                Field[] aimFields = dto.getDeclaredFields();
                List<Field> roleCombinedFields = Arrays.asList(aimFields).stream().filter(f -> f.getAnnotation(RoleCombined.class)!=null).collect(Collectors.toList());
                List<String> roleCombinedFieldNames = roleCombinedFields.stream().map(f -> f.getName()).collect(Collectors.toList());
                for(String rc_fieldName: roleCombinedFieldNames){
                    Field field = dto.getDeclaredField(rc_fieldName);
                    field.setAccessible(true);
                    Annotation roleCombinedObj = field.getAnnotation(RoleCombined.class);
                    if(roleCombinedObj == null) continue;
                    RoleCombined roleCombined = (RoleCombined)roleCombinedObj;

                    Permission fieldPermission = getPermission(pjp, roleCombined);
                    if(!userHasPermission(fieldPermission, loggedInUser)) {
                        BaseCRUDService crud = serviceFactory.getService(roleCombined.parent().getSimpleName());
                        //change id to annotation
                        Field idField = getField(AopUtils.getTargetClass(arg), "id");
                        idField.setAccessible(true);
                        Long id = idField.getLong(arg);
                        Object aim = crud.findOne(id);

                        //TODO: think if user whom priviliges has been taken shall loose also field depending on them?
                        if (aim != null) {
                            String fieldName = field.getName();
                            Object originalValue = new PropertyDescriptor(fieldName, aim.getClass()).getReadMethod().invoke(aim);
//                            if(originalValue == null) continue;
                            BaseCRUDService childCrud = serviceFactory.getService(roleCombined.child().getSimpleName());
                            //TODO: extend with set and think about maps
                            if(originalValue != null && List.class.isAssignableFrom(originalValue.getClass())){
                                List fieldList = new ArrayList();
                                List originalValueList = (List)originalValue;
                                for(Object o : originalValueList){
                                    Field collecionElementIdFieldId = getField(AopUtils.getTargetClass(o), "id");
                                    collecionElementIdFieldId.setAccessible(true);
                                    long collecionElementId = collecionElementIdFieldId.getLong(o);
                                    Object tempVal = childCrud.findOne(collecionElementId);
                                    fieldList.add(tempVal);
                                    field.set(arg, fieldList);
                                }
                            } else {
                                Class targetClass = getClassContainingField(AopUtils.getTargetClass(originalValue), "id");
                                if(targetClass!=null) {
                                    Object originalValueFieldId =
                                            new PropertyDescriptor("id", targetClass).getReadMethod().invoke(originalValue);
                                    originalValue = childCrud.findOne((long) originalValueFieldId);
                                    field.set(arg, originalValue);
                                }
                            }

                        } else {
                            GuardianConfig guardianConfigObj = field.getAnnotation(GuardianConfig.class);
                            if(guardianConfigObj != null){
                                GuardianConfig guardianConfig = (GuardianConfig)guardianConfigObj;
                                GuardianFinder guardianFinder = guardianFinderFactory.getGuardianFinder(guardianConfig.finder().getCanonicalName());
                                Object defaultValue = guardianFinder.find(roleCombined.child(), guardianConfig.key());
                                if(defaultValue == null) continue;
                                Class type = field.getType();
                                if(Collection.class.isAssignableFrom(type)){
                                    if(type.isInterface()){
                                        List list = new ArrayList();
                                        list.add(defaultValue);
                                        field.set(arg, list);
                                    } else {
                                        Object collectionObj = type.newInstance();
                                        Collection collection = (Collection) collectionObj;
                                        collection.add(defaultValue);
                                        field.set(arg, collection);
                                    }
                                } else {
                                    field.set(arg, defaultValue);
                                }
                            } else {
                                field.set(arg, null);
                            }
                        }
                    }
                }
                Object result = pjp.proceed(new Object[]{arg});
                return result;
            }
            else {
                return null;
            }

        } catch (ExecutionException e){
            e.printStackTrace();
            return null;
        } finally {

        }
    }

    private Field getField(Class argClass, String fieldName) throws NoSuchFieldException {
        Class classWithField = getClassContainingField(argClass, fieldName);
        if(classWithField == null || classWithField.equals(Object.class)) return null;
        Field searchedField = classWithField.getDeclaredField(fieldName);
        return searchedField;
    }

    private Class getClassContainingField(Class argClass, String fieldName){
        List<Field> fieldList = Arrays.asList(argClass.getDeclaredFields());
        boolean containsField = fieldList.stream().anyMatch(f -> f.getName().equals(fieldName));
        if(!containsField && !argClass.equals(Object.class)){
            argClass = argClass.getSuperclass();
            argClass = getClassContainingField(argClass, fieldName);
        }
        return argClass;
    }

    private Class getDto(ProceedingJoinPoint pjp){
        BaseCRUDServiceImpl CRUD = (BaseCRUDServiceImpl)pjp.getTarget();
        Class CRUD_cl = CRUD.getClass();
        Type genericSuperClass = CRUD_cl.getGenericSuperclass();

        if(genericSuperClass != null && (genericSuperClass instanceof ParameterizedType)){
            ParameterizedType pt = (ParameterizedType)genericSuperClass;
            if(pt != null && pt.getActualTypeArguments().length > 0){
                Class dto_cl = (Class)pt.getActualTypeArguments()[0];
                return dto_cl;
            }
        }
        return null;
    }

    private boolean elementOnStackTrace(StackTraceElementQualifier etec){
        StackTraceElement[] stList = Thread.currentThread().getStackTrace();
        return Arrays.asList(stList).stream().filter(e -> etec.process(e)).collect(Collectors.toSet()).size() > 0;
    }

    //TODO: analyze if really needed
    private boolean securityOnStackTrace(){
        boolean contains = elementOnStackTrace(new StackTraceElementQualifier() {
            @Override
            public boolean process(StackTraceElement el1) {
                String stackElMethodName = el1.getMethodName();
                String securityUserLoaderMethodName = "loadUserByUsername";
                if(stackElMethodName.equals(securityUserLoaderMethodName)){
                    return true;
                }
                return false;
            }
        });
        return contains;
    }

    private boolean isControlledByGuardian(ProceedingJoinPoint pjp) throws NoSuchMethodException {

        if(GuardianControlled.class.isAssignableFrom(pjp.getSignature().getDeclaringType())){
            GuardianConfig guardianConfig = getGuardianConfig(pjp);
            if(guardianConfig != null && guardianConfig.NotControlled()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static Method getMethodForGuardian(ProceedingJoinPoint pjp) throws NoSuchMethodException {
        Class[] args = getArgsForGuardian(pjp);
        Method m = pjp.getTarget().getClass().getMethod(pjp.getSignature().getName(), args);
        return m;
    }

    private pl.altemic.auctionhouse.guardian.PermissionType getPermissionType(ProceedingJoinPoint pjp) throws NoSuchMethodException {
        Method m = getMethodForGuardian(pjp);
        pl.altemic.auctionhouse.guardian.PermissionType permissionTypeAnnotation = m.getAnnotation(pl.altemic.auctionhouse.guardian.PermissionType.class);
        return permissionTypeAnnotation;
    }

    private PermissionType getDtoPermissiontype(ProceedingJoinPoint pjp) throws NoSuchMethodException {
        pl.altemic.auctionhouse.guardian.PermissionType guardianPermissionType = getPermissionType(pjp);
        if(guardianPermissionType!=null){
            return guardianPermissionType.permissionType();
        }
        return PermissionType.VIEW;
    }

    private pl.altemic.auctionhouse.guardian.GuardianConfig getGuardianConfig(ProceedingJoinPoint pjp) throws NoSuchMethodException {
        Method m = getMethodForGuardian(pjp);
        //pl.altemic.auctionhouse.guardian.GuardianConfig guardianConfig = m.getAnnotation(pl.altemic.auctionhouse.guardian.GuardianConfig.class);
        pl.altemic.auctionhouse.guardian.GuardianConfig guardianConfig = AnnotationUtils.findAnnotation(m, pl.altemic.auctionhouse.guardian.GuardianConfig.class);
        return guardianConfig;
    }

    private static Class[] getArgsForGuardian(ProceedingJoinPoint pjp){
        Class[] args = pjp.getArgs() != null ? Arrays.asList(pjp.getArgs()).stream().map(
                o -> Common.isPrimitive(o.getClass()) ? o.getClass() :(
                        (Object[].class.isAssignableFrom(o.getClass())) ? Object[].class : Object.class))
                .collect(Collectors.toList()).toArray(new Class[pjp.getArgs().length]) : null;
        return args;
    }

    private Permission getPermission(ProceedingJoinPoint pjp, RoleCombined roleCombined) {
        Class dto = getDto(pjp);
        try {
            pl.altemic.auctionhouse.guardian.PermissionType permissionType = getPermissionType(pjp);
            String permissionTypeName;
            if(permissionType == null){
                permissionTypeName = PermissionType.VIEW.toString();
            } else {
                permissionTypeName = permissionType.permissionType().name();
            }
            String reference = dto.getSimpleName();
            if(roleCombined!=null){
                Class child = roleCombined.child();
                Class parent = roleCombined.parent();
                reference = parent.getSimpleName() + "_" + child.getSimpleName();
            }
            //PermissionService permissionService = serviceFactory.getService(Permission.class.getSimpleName());
            Permission permission = permissionModule.findByTypeAndReference(permissionTypeName, reference.toUpperCase());
            return permission;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        } /* catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } */
    }

    private boolean userIsOwnerPjp(ProceedingJoinPoint pjp, User user){

        Ownable owner = getOwner(pjp);

        if(owner != null) {
            if (user.getGroup() != null) {
                if (user.getGroup().getId() == owner.getId()) return true;
                if(user.getId() == owner.getId()) return true;
            } else {
                if(user.getId() == owner.getId()) return true;
            }
        }

        return false;
    }

    private Ownable getOwner(ProceedingJoinPoint pjp){
        Class dto = getDto(pjp);
        if(pjp != null && Ownable.class.isAssignableFrom(dto)){
            Ownable ownable = (Ownable)pjp.getArgs()[0];
            Ownable owner = ownable.getOwner();

            while(owner!=null && !(User.class.isAssignableFrom(owner.getClass())  || Group.class.isAssignableFrom(owner.getClass()))){
                owner = owner.getOwner();
            }
            return owner;
        }
        return null;
    }

    private boolean userHasPermission(Permission requiredPermission, User user) throws ClassNotFoundException {
        if(requiredPermission==null || user == null) return false;
        List<Permission> permissionList = permissionModule.getPermissionsForUser(user);
        boolean contains = permissionList.stream()
                .anyMatch(p -> p.getType().equals(requiredPermission.getType()) && p.getReference().equals(requiredPermission.getReference()));
        return contains;
    }
}

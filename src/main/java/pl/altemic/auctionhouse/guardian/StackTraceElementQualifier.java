package pl.altemic.auctionhouse.guardian;

/**
 * Created by m.altenberg on 24.01.2017.
 */
public interface StackTraceElementQualifier {
    boolean process(StackTraceElement stel);
}

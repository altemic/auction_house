package pl.altemic.auctionhouse.guardian;

import pl.altemic.auctionhouse.dto.OwnableException;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.utils.Identity;

/**
 * Created by m.altenberg on 21.01.2017.
 */
public interface Ownable<T extends Ownable> extends Identity{
    Ownable getOwner();
    void setOwner(T ownable) throws OwnableException;
    long getId();
    void setId(long id);
}

package pl.altemic.auctionhouse.guardian;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by m.altenberg on 16.12.2016.
 */

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface GuardianConfig {
    boolean NotControlled() default false;
    Class<? extends GuardianFinder> finder() default GuardianFinderBasicImpl.class;
    String[] key() default "id:-1";
    String[] finderParameter() default "";
}

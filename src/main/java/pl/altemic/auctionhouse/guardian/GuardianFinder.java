package pl.altemic.auctionhouse.guardian;

/**
 * Created by m.altenberg on 28.01.2017.
 */
public interface GuardianFinder {
    Object find(Class dto, String[] params);
    String getFinderName();
}

package pl.altemic.auctionhouse.guardian;

/**
 * Created by m.altenberg on 29.01.2017.
 */
public interface GuardianFinderFactory {
    GuardianFinder getGuardianFinder(String name) throws ClassNotFoundException;
}

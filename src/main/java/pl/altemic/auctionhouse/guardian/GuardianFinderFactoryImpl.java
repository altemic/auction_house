package pl.altemic.auctionhouse.guardian;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by m.altenberg on 29.01.2017.
 */
@Repository
public class GuardianFinderFactoryImpl implements GuardianFinderFactory {

    List<GuardianFinder> guardianFinders;

    @Autowired
    public void setGuardianFinders(@Lazy List<GuardianFinder> guardianFinders) {
        this.guardianFinders = guardianFinders;
    }

    @Override
    public GuardianFinder getGuardianFinder(String name) throws ClassNotFoundException {
        for(GuardianFinder guardianFinder : guardianFinders){
            if(guardianFinder.getFinderName().equals(name)){
                return guardianFinder;
            }
        }

        throw new ClassNotFoundException("Guardian finder with name: " + name + " hasn't been found.");
    }
}

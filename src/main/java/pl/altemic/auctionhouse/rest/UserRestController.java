package pl.altemic.auctionhouse.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.modules.UserModule;
import pl.altemic.auctionhouse.modules.tools.PasswordTools;
import pl.altemic.auctionhouse.optimizer.LazyRemover;
import pl.altemic.auctionhouse.security.ContextUser;

/**
 * Created by m.altenberg on 22.12.2016.
 */

@RestController
@Transactional
@RequestMapping("/api/users")
public class UserRestController {

    UserModule userModule;
    PasswordTools passwordTools;
    ContextUser contextUser;

    @Autowired
    public void setUserModule(UserModule userModule) {
        this.userModule = userModule;
    }

    @Autowired
    public void setPasswordTools(PasswordTools passwordTools) {
        this.passwordTools = passwordTools;
    }

    @Autowired
    public void setContextUser(ContextUser contextUser) {
        this.contextUser = contextUser;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public User getUserByUsername(@RequestParam String username) throws Exception {
        if(username == null) return null;
        User u = userModule.findByUsername(username);
        u = LazyRemover.extract(u);
        return u;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, path = "/{id}")
    public User getUserById(@PathVariable Long id) throws Exception {
        if(id == null) return null;
        User u = userModule.findOne(id);
        u = LazyRemover.extract(u);
        return u;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE, path = "/checkPasswordStrength")
    public boolean isPasswordStrong(@RequestParam(name = "passwordCandidate") String passwordCandidate){
        return passwordTools.matchesRequirements(passwordCandidate);
    }
}

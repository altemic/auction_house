package pl.altemic.auctionhouse.utils;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by michal on 13.11.2016.
 */
public class Common {

    public static boolean isPrimitive(Class<?> c){
        if(c.isPrimitive()) return true;
        return getWrapperTypes().contains(c);
    }

    private static Set<Class<?>> getWrapperTypes()
    {
        Set<Class<?>> ret = new HashSet<>();

        ret.add(String.class);

        ret.add(Boolean.class);
        ret.add(Character.class);
        ret.add(Byte.class);
        ret.add(Short.class);
        ret.add(Integer.class);
        ret.add(Long.class);
        ret.add(Float.class);
        ret.add(Double.class);
        ret.add(Void.class);
        return ret;
    }

    public static boolean isNullOrDefaultValue(Object aim){
        if(aim == null) return true;
        List<Object> defValues = getDefaultValues(aim.getClass());
        for(Object v : defValues){
            if(v.equals(aim)) return true;

        }
        return false;
    }

    private static List<Object> getDefaultValues(Class c){
        List<Entry> vm = new ArrayList<>();

        vm.add(new Entry(String.class, ""));
        vm.add(new Entry(Long.class, Long.valueOf(0)));
        vm.add(new Entry(long.class, Long.valueOf(0)));
        vm.add(new Entry(Boolean.class, false));
        vm.add(new Entry(boolean.class, false));
        vm.add(new Entry(Character.class, false));
        vm.add(new Entry(Byte.class, false));
        vm.add(new Entry(Short.class, false));
        vm.add(new Entry(short.class, false));
        vm.add(new Entry(Integer.class, false));
        vm.add(new Entry(int.class, false));
        vm.add(new Entry(Float.class, false));
        vm.add(new Entry(float.class, false));
        vm.add(new Entry(Double.class, false));
        vm.add(new Entry(double.class, false));
        vm.add(new Entry(void.class, false));
        vm.add(new Entry(Void.class, false));

        List<Object> defValues = vm.stream().filter(d -> d.getKey().equals(c)).map(Entry::getValue).collect(Collectors.toList());
        return defValues;
    }

    private static class Entry{
        Class key;
        Object value;

        public Entry(Class c, Object v){
            key = c;
            value = v;
        }

        public Class getKey() {
            return key;
        }

        public void setKey(Class key) {
            this.key = key;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }
    }
}

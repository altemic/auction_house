package pl.altemic.auctionhouse.utils;

import pl.altemic.auctionhouse.dto.BaseDto;

import java.util.Collection;
import java.util.List;

/**
 * Created by m.altenberg on 23.02.2017.
 */
public interface IdentityComparator {
    <T extends Identity,V extends Identity> boolean areTheSame(T i1, V i2);
    <T extends Identity,V extends Identity> boolean areTheSame(Collection<T> ci1, Collection<V> ci2);
}

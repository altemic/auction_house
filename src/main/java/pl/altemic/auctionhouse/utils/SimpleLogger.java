package pl.altemic.auctionhouse.utils;

import org.springframework.stereotype.Component;

/**
 * Created by m.altenberg on 15.03.2017.
 */

@Component
public class SimpleLogger implements Logger{
	
	public final int INFO = 10;
	public final int ERROR = 10;
	
	public void log(String message, int level){
		
	}
	
    public void info(String message){
		log(message, INFO);		
	}
	
    public void error(String message){
		log(message, ERROR);
	}
}

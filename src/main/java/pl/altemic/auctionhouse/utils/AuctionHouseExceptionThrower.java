package pl.altemic.auctionhouse.utils;

/**
 * Created by m.altenberg on 21.02.2017.
 */
public interface AuctionHouseExceptionThrower {
    void throwException(String message) throws AuctionHouseException;
}

package pl.altemic.auctionhouse.utils;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 * Created by m.altenberg on 29.01.2017.
 */
public class CollectorUtils {
    class ListCollector<T> implements Collector<List<T>, List<T>, List<T>> {

        @Override
        public Supplier<List<T>> supplier() {
            return () -> new ArrayList<T>();
        }

        @Override
        public BiConsumer<List<T>, List<T>> accumulator() {
            return List::addAll;
        }

        @Override
        public BinaryOperator<List<T>> combiner() {
            return (listA, listB) -> {
                listA.addAll(listB);
                return listA;
            };
        }

        @Override
        public Function<List<T>, List<T>> finisher() {
            return (x) -> x;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return EnumSet.of(Characteristics.UNORDERED);
        }
    }
}

package pl.altemic.auctionhouse.utils;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.models.Owner;
import pl.altemic.auctionhouse.services.BaseCRUDServiceImpl;
import pl.altemic.auctionhouse.services.Service;
import pl.altemic.auctionhouse.services.ServiceFactory;

/**
 * Created by m.altenberg on 08.02.2017.
 */

@Component
public class OwnableConverterImpl implements OwnableConverter{

    ServiceFactory serviceFactory;

    @Autowired
    public void setServiceFactory(@Lazy ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    public <T, V> BaseCRUDServiceImpl<T, V> find (String name) throws ClassNotFoundException {
        Service service = serviceFactory.getService(name);

        if(!(service instanceof BaseCRUDServiceImpl)) return null;
        //Verify compatiblity of types
        BaseCRUDServiceImpl<T, V> baseCRUD = (BaseCRUDServiceImpl<T, V>)service;
        return baseCRUD;
    }

    @Override
    public <T extends Ownable,V extends Owner> V FromDtoToModel(T dto) throws ClassNotFoundException {
        if(dto == null) return null;
        BaseCRUDServiceImpl<T,V> CRUD = find(AopUtils.getTargetClass(dto).getSimpleName());
        Object model = CRUD.FromDtoToModel(dto);
        return (V)model;
    }

    @Override
    public <T extends Ownable, V extends Owner> T FromModelToDto(V model) throws ClassNotFoundException {
        if(model == null) return null;
        BaseCRUDServiceImpl<T,V> CRUD = find(AopUtils.getTargetClass(model).getSimpleName());
        Object dto = CRUD.FromModelToDto(model);
        return (T)dto;
    }
}

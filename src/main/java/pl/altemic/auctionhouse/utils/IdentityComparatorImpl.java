package pl.altemic.auctionhouse.utils;

import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Created by m.altenberg on 23.02.2017.
 */
@Component
public class IdentityComparatorImpl implements IdentityComparator {

    @Override
    public <T extends Identity, V extends Identity> boolean areTheSame(T i1, V i2) {
        if(i1 == null && i2 == null) return true;
        if(i1 == null || i2 == null) return false;
        if(i1.getId() == i2.getId()) return true;
        return false;
    }

    @Override
    public <T extends Identity, V extends Identity> boolean areTheSame(Collection<T> ci1, Collection<V> ci2) {
        if(ci1==null && ci2==null) return true;
        if(ci1==null || ci2==null) return false;
        boolean theSame = ci1.size() == ci2.size() && ci1.stream().allMatch(i -> ci2.stream().anyMatch(j -> j.getId() == i.getId()));
        return theSame;
    }

}

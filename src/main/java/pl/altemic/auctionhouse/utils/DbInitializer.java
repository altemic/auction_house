package pl.altemic.auctionhouse.utils;

/**
 * Created by michal on 21.11.2016.
 */
public interface DbInitializer {
    void process();
}

package pl.altemic.auctionhouse.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 13.11.2016.
 */


@Component
public class SimpleMapper<T,V> implements Mapper<T,V> {

    private List<Field> getAllFields(Class c){
        List<Field> fields = Arrays.asList(c.getDeclaredFields());
        fields = new ArrayList<>(fields);
        while(!c.getSuperclass().equals(Object.class)){
            c = c.getSuperclass();
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        return fields;
    }

    @Override
    public V Convert(T initial, V result) {
        try {
//            Class initialClass = AopUtils.getTargetClass(initial);
            List<Field> initialObjectFieldsList = getAllFields(initial.getClass());
            List<Field> resultObjectFieldsList = getAllFields(result.getClass());;

            for (Field initialObjField : initialObjectFieldsList) {
                MapperInfo mapperInfo = initialObjField.getAnnotation(MapperInfo.class);
                if(mapperInfo!=null){
                    if(mapperInfo.Ignore()) continue;
                }
                if(!(Common.isPrimitive(initialObjField.getType()) || initialObjField.getType().equals(Timestamp.class))){
                    continue;
                }
                initialObjField.setAccessible(true);
                String name = initialObjField.getName();
                Object value = initialObjField.get(initial);

                if(Common.isNullOrDefaultValue(value)) {
                    PropertyDescriptor pd = BeanUtils.getPropertyDescriptor(initial.getClass(), name);
                    if (pd != null) {
                        Method getter = pd.getReadMethod();
                        Object getterValue = getter.invoke(initial);
                        if (getterValue != null && !Common.isNullOrDefaultValue(getterValue)) value = getterValue;
                    }
                }

                if (resultObjectFieldsList.stream().anyMatch(x -> (x.getName().equals(name)) && x.getType().equals(initialObjField.getType()))) {
                    List<Field> resultObjFields = resultObjectFieldsList.stream().filter(x -> x.getName().equals(name)).collect(Collectors.toList());
                    Field f = resultObjFields.get(0);
                    f.setAccessible(true);
                    f.set(result, value);
                }
            }
        } catch (IllegalAccessException e){
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }
}

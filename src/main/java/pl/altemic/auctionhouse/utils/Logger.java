package pl.altemic.auctionhouse.utils;

/**
 * Created by m.altenberg on 15.03.2017.
 */

public interface Logger {
    void log(String message, int level);
    void info(String message);
    void error(String message);
}

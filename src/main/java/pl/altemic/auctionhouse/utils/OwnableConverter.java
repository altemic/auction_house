package pl.altemic.auctionhouse.utils;

import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.models.Owner;

/**
 * Created by m.altenberg on 09.02.2017.
 */
public interface OwnableConverter {
    <T extends Ownable,V extends Owner> V FromDtoToModel(T dto) throws ClassNotFoundException;
    <T extends Ownable,V extends Owner> T FromModelToDto(V model) throws ClassNotFoundException;
}

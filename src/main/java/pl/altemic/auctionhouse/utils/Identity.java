package pl.altemic.auctionhouse.utils;

/**
 * Created by m.altenberg on 23.02.2017.
 */
public interface Identity {
    long getId();
}

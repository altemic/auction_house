package pl.altemic.auctionhouse.utils;

/**
 * Created by michal on 13.11.2016.
 */
public interface Mapper<T,V> {
    V Convert(T initial, V result);
}

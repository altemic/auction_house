package pl.altemic.auctionhouse.utils;

/**
 * Created by m.altenberg on 26.01.2017.
 */
public class Constants {
    public static String ANONUMOUS_USERNAME = "anonymous";
    public static String ANONYMOUS_GROUPTYPE = "ANONYMOUS_GROUPTYPE";
    public static String ANONYMOUS_GROUP = "ANONYMOUS_GROUP";
    public static String ADMIN_GROUPTYPE = "ADMIN_GROUPTYPE";
    public static String ADMIN_GROUP = "ADMIN_GROUP";
    public static String ADMIN_ROLE_SUFFIX = "ADMIN";
    public static String NORMAL_USER_GROUPTYPE = "NORMAL_USER_GROUPTYPE";
}

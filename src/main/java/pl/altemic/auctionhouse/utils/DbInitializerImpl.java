package pl.altemic.auctionhouse.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import pl.altemic.auctionhouse.dto.*;
import pl.altemic.auctionhouse.dto.Dictionary;
import pl.altemic.auctionhouse.guardian.RoleCombined;
import pl.altemic.auctionhouse.modules.*;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by michal on 21.11.2016.
 */
@Component
public class DbInitializerImpl implements DbInitializer {

    public static boolean firstRun = true;

    RoleModule roleModule;
    GroupTypeModule groupTypeModule;
    GroupModule groupModule;
    UserModule userModule;
    ConfigModule configModule;
    PermissionModule permissionModule;
    DictionaryModule dictionaryModule;

    @Autowired
    public void setRoleModule(@Lazy RoleModule roleModule) {
        this.roleModule = roleModule;
    }

    @Autowired
    public void setGroupTypeModule(@Lazy GroupTypeModule groupTypeModule) {
        this.groupTypeModule = groupTypeModule;
    }

    @Autowired
    public void setGroupModule(@Lazy GroupModule groupModule) {
        this.groupModule = groupModule;
    }

    @Autowired
    public void setUserModule(@Lazy UserModule userModule) {
        this.userModule = userModule;
    }

    @Autowired
    public void setConfigModule(@Lazy ConfigModule configModule) {
        this.configModule = configModule;
    }

    @Autowired
    public void setPermissionModule(@Lazy PermissionModule permissionModule) {
        this.permissionModule = permissionModule;
    }

    @Autowired
    public void setDictionaryModule(DictionaryModule dictionaryModule) {
        this.dictionaryModule = dictionaryModule;
    }

    private interface Init{
        boolean Execute();
    }

    private void Initialise(Map<Integer, Init> steps){
        Map<Integer, Init> orderedMap =
                steps.entrySet().stream().sorted(Map.Entry.comparingByKey(
                        (o1, o2) -> o1 > o2 ? 1 : -1
                )).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        for (Map.Entry step : orderedMap.entrySet()){
            Init obj = (Init)step.getValue();
            boolean success = obj.Execute();
            if(!success) return;
        }
    }

    public void process(){
        Settings settings = configModule.load();
        firstRun = false;
        if(settings.getInitDate() == ""){
            firstRun = true;
        }

        if(!firstRun) return;

        //Initialisation
        settings.setInitDate(new Timestamp(Calendar.getInstance().getTimeInMillis()).toString());
        configModule.save(settings);

        Map<Integer, Init> initParts = new LinkedHashMap();

        //1. Basic permissions and roles for all db models
        initParts.put(1, () -> addPermissionsAndRoles());
        initParts.put(2, () -> addDictionaries());

        //Execute
        Initialise(initParts);

        firstRun = false;
    }

    private Permission createPermission(String reference, PermissionType permissionType) throws AuctionHouseException {
        reference = reference.toUpperCase();
        Permission permission = new Permission();
        permission.setReference(reference);
        permission.setType(permissionType);
        String name = reference.toUpperCase() + "_" + permissionType;
        permission.setName(name);
        permissionModule.add(permission);
        return permissionModule.findByTypeAndReference(permissionType, reference);
    }

    private Role createAdminRoleForDto(Class dto) throws AuctionHouseException {
        return createRoleForDto(dto, "ADMIN", PermissionType.values());
    }

    private Role createRoleForDto(Class dto,String suffix, PermissionType[] permissionTypes) throws AuctionHouseException {
        Role role = new Role();
        String name = dto.getSimpleName().toUpperCase() + "_" + suffix;
        role.setName(name);
        role.setPermissionList(new ArrayList<>());
        for(PermissionType permissionType : permissionTypes){
            Permission permission = permissionModule.findByTypeAndReference(permissionType, dto.getSimpleName().toUpperCase());
            if(permission == null) {
                permission = createPermission(dto.getSimpleName(), permissionType);
            }
            role.getPermissionList().add(permission);
            List<Permission> guardianAnnotatedPermissions = createPermissionForGuardianAnnontatedFields(dto, permissionType);
            role.getPermissionList().addAll(guardianAnnotatedPermissions);
        }
        roleModule.add(role);
        return roleModule.findByName(name);
    }

    private List<Permission> createPermissionForGuardianAnnontatedFields(Class dto, PermissionType permissionType) throws AuctionHouseException {
        List<Field> dtoRoleCOmbinedAnnotatedFields = Arrays.asList(dto.getDeclaredFields()).stream().filter(f -> f.getAnnotation(RoleCombined.class)!=null).collect(Collectors.toList());
        List<Permission> permissions = new ArrayList<>();
        for(Field f : dtoRoleCOmbinedAnnotatedFields){
            RoleCombined rc = (RoleCombined)f.getAnnotation(RoleCombined.class);
            Class child = rc.child();
            Class parent = rc.parent();
            String reference = parent.getSimpleName().toUpperCase() + "_" + child.getSimpleName().toUpperCase();
            String name = reference + "_" + permissionType;
            Permission permission = permissionModule.findByTypeAndReference(permissionType, dto.getSimpleName().toUpperCase());
            if(permission == null) {
                permission = new Permission();
                permission.setName(name);
                permission.setReference(reference);
                permission.setType(permissionType);
                permissionModule.add(permission);
                permission = permissionModule.findByTypeAndReference(permissionType, reference);
            }
            permissions.add(permission);
        }
        return permissions;
    }

    private GroupType createAdminGroupType(String name) throws AuctionHouseException {
        Set<Class> modelClasses = ReflectionHelper.getClassesInPackage("pl.altemic.auctionhouse.dto");
        modelClasses = modelClasses.stream().filter(
                c -> c.getAnnotation(DbInitializerConfig.class) == null
                        || (c.getAnnotation(DbInitializerConfig.class) != null &&
                        ((DbInitializerConfig)c.getAnnotation(DbInitializerConfig.class)).createPermissions())).collect(Collectors.toSet());
        GroupType adminGroupType = new GroupType();
        adminGroupType.setRoleList(new ArrayList<>());
        adminGroupType.setName(name);
        for (Class model : modelClasses) {
            Role role = createAdminRoleForDto(model);
            adminGroupType.getRoleList().add(role);
        }
        groupTypeModule.add(adminGroupType);
        return groupTypeModule.findByName(name);
    }

    private Group createAdminGroup(String name) throws AuctionHouseException {
        Group group = new Group();
        group.setName(name);
        group.setGroupTypes(new ArrayList<>());
        group.setGroupTypes(Arrays.asList(createAdminGroupType(Constants.ADMIN_GROUPTYPE)));
        groupModule.add(group);
        return groupModule.findByName(name);
    }

    private boolean addPermissionsAndRoles(){
        try {

            User adminUser = new User();
            adminUser.setUsername("admin");
            adminUser.setPassword("admin");
            Group adminGroup = createAdminGroup(Constants.ADMIN_GROUP);
            adminUser.setGroup(adminGroup);
            userModule.add(adminUser);


            //Responsibilities of anonymous user -> not signed in user
            //1.Register new user
            //A) Create User
            //B) Create Group
            //C) Assign Group to basic grouptype (Potential risk of adding new user to admin group -> maybe add RoleCombined to grouptype?)

            GroupType anonymousGroupType = new GroupType();
            anonymousGroupType.setName(Constants.ANONYMOUS_GROUPTYPE);
            anonymousGroupType.setRoleList(new ArrayList<>());
            anonymousGroupType.getRoleList().add(createRoleForDto(User.class, "ADD", new PermissionType[] {PermissionType.ADD}));
            anonymousGroupType.getRoleList().add(createRoleForDto(Group.class, "ADD", new PermissionType[] {PermissionType.ADD}));
            groupTypeModule.add(anonymousGroupType);
            anonymousGroupType = groupTypeModule.findByName(Constants.ANONYMOUS_GROUPTYPE);

            Group anonymousGroup = new Group();
            anonymousGroup.setName(Constants.ANONYMOUS_GROUP);
            anonymousGroup.setGroupTypes(new ArrayList<>());
            anonymousGroup.getGroupTypes().add(anonymousGroupType);
            groupModule.add(anonymousGroup);
            anonymousGroup = groupModule.findByName(Constants.ANONYMOUS_GROUP);

            User anonymousUser = new User();
            anonymousUser.setUsername(Constants.ANONUMOUS_USERNAME);
            anonymousUser.setPassword(Constants.ANONUMOUS_USERNAME);
            anonymousUser.setGroup(anonymousGroup);
            userModule.add(anonymousUser);

            GroupType normalUserGroupType = new GroupType();
            normalUserGroupType.setName(Constants.NORMAL_USER_GROUPTYPE);
            normalUserGroupType = groupTypeModule.add(normalUserGroupType);

            //SUCCEESS
            return true;
        } catch (Exception e){
            e.printStackTrace();
        }
        //ERROR
        return false;
    }

    public boolean addDictionaries(){
        String[] dictionarySymbols = new String[] {"PL"};
        for(String dictSymbol : dictionarySymbols){
            try {
                addDictionary(dictSymbol);
            } catch (DictionaryModule.DictionaryException e) {
                e.printStackTrace();
                return false;
            } catch (AuctionHouseException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public void addDictionary(String symbol) throws AuctionHouseException {
        Dictionary dictionary = new Dictionary();
        dictionary.setSymbol(symbol);
        dictionaryModule.add(dictionary);
    }
}

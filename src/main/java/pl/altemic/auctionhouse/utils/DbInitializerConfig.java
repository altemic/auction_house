package pl.altemic.auctionhouse.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by m.altenberg on 21.01.2017.
 */
 
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DbInitializerConfig {
    boolean createPermissions() default true;
}

package pl.altemic.auctionhouse.utils;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by michal on 20.11.2016.
 */
public class Settings {
    public String initDate;

    public Settings(){
        initDate = "";/*new Timestamp(Calendar.getInstance().getTimeInMillis()).toString()*/;
    }

    public String getInitDate() {
        return initDate;
    }

    public void setInitDate(String initDate) {
        this.initDate = initDate;
    }
}

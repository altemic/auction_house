package pl.altemic.auctionhouse.utils;

/**
 * Created by m.altenberg on 15.12.2016.
 */
public class AuctionHouseException extends Exception {
    public AuctionHouseException(){}

    public AuctionHouseException(String message){
        super(message);
    }
}

package pl.altemic.auctionhouse.utils;

/**
 * Created by michal on 15.11.2016.
 */
public class GenericsMock<T> {
    final Class<T> typeParameterClass;

    public GenericsMock(Class<T> classToWrap){
        this.typeParameterClass = classToWrap;
    }

    public Class getWrappedClass(){
        return typeParameterClass;
    }

    public Class<T> getType(){ return typeParameterClass; }
}

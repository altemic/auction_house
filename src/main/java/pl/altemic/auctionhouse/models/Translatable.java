package pl.altemic.auctionhouse.models;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * Created by michal on 13.11.2016.
 */
@MappedSuperclass
public abstract class Translatable extends BaseEntity{
    @ManyToOne
    @JoinColumn(name = "dictionary_id")
//    @Column(nullable = false)
    @NotNull
    Dictionary dictionary;

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }
}

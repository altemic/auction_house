package pl.altemic.auctionhouse.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
public class Role extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_role")
    long id;
    @Column(unique = true)
    String name;
    @ManyToMany
    @JoinTable(name="role_permissions",
            joinColumns = {@JoinColumn(name="role_id", referencedColumnName = "id_role")},
            inverseJoinColumns = {@JoinColumn(name="permission_id", referencedColumnName = "id_permission")})
    List<Permission> permissionList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Permission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }
}

package pl.altemic.auctionhouse.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by m.altenberg on 20.12.2016.
 */
@Entity
@DiscriminatorValue("CATEGORY")
@PrimaryKeyJoinColumn(name = "id_category", referencedColumnName = "owner_id")
public class Category extends Owner{
    @ManyToMany
    @JoinTable(name="category_categorytype",
            joinColumns = {@JoinColumn(name="category_id", referencedColumnName = "id_category")},
            inverseJoinColumns = {@JoinColumn(name="categorytype_id", referencedColumnName = "id_categorytype")})
    List<CategoryType> categoryTypeList;
    @OneToMany
    @JoinColumn(name="owner_id")
    List<Title> titleList;
    @OneToMany
    @JoinColumn(name="owner_id")
    List<Description> descriptionList;
    @Column(unique = true)
    String name;
    @ManyToMany
    @JoinTable(name="category_category",
            joinColumns = {@JoinColumn(name="childcategory_id", referencedColumnName = "id_category")},
            inverseJoinColumns = {@JoinColumn(name="parentcategory_id", referencedColumnName = "id_category")})
    List<Category> dependingCategoryList;

    public List<CategoryType> getCategoryTypeList() {
        return categoryTypeList;
    }

    public void setCategoryTypeList(List<CategoryType> categoryTypeList) {
        this.categoryTypeList = categoryTypeList;
    }

    public List<Title> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<Title> titleList) {
        this.titleList = titleList;
    }

    public List<Description> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(List<Description> descriptionList) {
        this.descriptionList = descriptionList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getDependingCategoryList() {
        return dependingCategoryList;
    }

    public void setDependingCategoryList(List<Category> dependingCategoryList) {
        this.dependingCategoryList = dependingCategoryList;
    }
}

package pl.altemic.auctionhouse.models;

import javax.persistence.*;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
public class Currency extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_currency")
    long id;
    String symbol;
    double exchange; //shall be historical

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getExchange() {
        return exchange;
    }

    public void setExchange(double exchange) {
        this.exchange = exchange;
    }
}

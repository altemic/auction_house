package pl.altemic.auctionhouse.models;

import javax.persistence.*;

/**
 * Created by michal on 20.11.2016.
 */

@Entity
public class Config extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_config")
    long id;
    @Column(unique = true)
    String name;
    String value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

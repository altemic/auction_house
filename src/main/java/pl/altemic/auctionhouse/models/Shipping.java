package pl.altemic.auctionhouse.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
@DiscriminatorValue("SHIPPING")
@PrimaryKeyJoinColumn(name = "id_shipping", referencedColumnName = "owner_id")
public class Shipping extends Owner{
    @OneToOne
    @JoinColumn(name="price_id")
    Price price;
    @ManyToOne
    @JoinColumn(name="auction_id")
    Auction auction;
    @OneToMany
    @JoinColumn(name="owner_id")
    List<Title> titleList;
    @OneToMany
    @JoinColumn(name="owner_id")
    List<Description> descriptionList;

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public List<Title> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<Title> titleList) {
        this.titleList = titleList;
    }

    public List<Description> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(List<Description> descriptionList) {
        this.descriptionList = descriptionList;
    }
}

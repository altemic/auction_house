package pl.altemic.auctionhouse.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
public class Description extends Translatable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_description")
    long id;
    String text;
    @ManyToOne
    @JoinColumn(name="owner_id")
    Owner owner;

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

package pl.altemic.auctionhouse.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
@DiscriminatorValue("AUCTION")
@PrimaryKeyJoinColumn(name = "id_auction", referencedColumnName = "owner_id")
public class Auction extends Owner{
    @OneToMany
    @JoinColumn(name="owner_id")
    List<Title> titleList;
    @OneToMany
    @JoinColumn(name="owner_id")
    List<Description> descriptionList;
    @ManyToOne
    @JoinColumn(name = "seller_id")
    User seller;
    @OneToMany(mappedBy = "auction")
    List<Offer> offerList;
    @OneToMany
    @JoinColumn(name="auction_id")
    List<Shipping> shippingList;
    @ManyToOne
    @JoinColumn(name = "category_id")
    Category category;

    public List<Description> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(List<Description> descriptionList) {
        this.descriptionList = descriptionList;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public List<Offer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<Offer> offerList) {
        this.offerList = offerList;
    }

    public List<Title> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<Title> titleList) {
        this.titleList = titleList;
    }

    public List<Shipping> getShippingList() {
        return shippingList;
    }

    public void setShippingList(List<Shipping> shippingList) {
        this.shippingList = shippingList;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}

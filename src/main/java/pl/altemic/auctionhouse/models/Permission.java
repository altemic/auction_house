package pl.altemic.auctionhouse.models;

import pl.altemic.auctionhouse.utils.MapperInfo;

import javax.persistence.*;
/**
 * Created by michal on 08.11.2016.
 */
@Entity
public class Permission extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_permission")
    long id;
    String name;
    @MapperInfo(Ignore = true)
    int type;
    String reference;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}

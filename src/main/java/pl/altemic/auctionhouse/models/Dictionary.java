package pl.altemic.auctionhouse.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
public class Dictionary extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_dictionary")
    long id;
    @Column(nullable = false, unique = true)
    @NotNull
    String symbol;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}

package pl.altemic.auctionhouse.models;

/**
 * Created by michal on 08.11.2016.
 */

import javax.persistence.*;
import java.util.List;

@Entity
public class Payment extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_payment")
    long id;
    @OneToMany
    @JoinColumn(name="payment_id")
    List<PaymentStatus> paymentStatuses;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<PaymentStatus> getPaymentStatuses() {
        return paymentStatuses;
    }

    public void setPaymentStatuses(List<PaymentStatus> paymentStatuses) {
        this.paymentStatuses = paymentStatuses;
    }
}

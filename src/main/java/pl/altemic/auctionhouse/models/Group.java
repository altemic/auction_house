package pl.altemic.auctionhouse.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
@Table(name="\"group\"")
@DiscriminatorValue("GROUP")
@PrimaryKeyJoinColumn(name = "id_group", referencedColumnName = "owner_id")
public class Group extends Owner{
    @Column(unique = true)
    String name;
    @ManyToMany
    @JoinTable(name="group_grouptype",
            joinColumns = {@JoinColumn(name="group_id", referencedColumnName = "id_group")},
            inverseJoinColumns = {@JoinColumn(name="grouptype_id", referencedColumnName = "id_grouptype")})
    List<GroupType> groupTypes;
    @OneToMany(mappedBy = "group")
    List<BillingData> bilingData;
    @OneToMany(mappedBy = "group")
    List<User> userList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GroupType> getGroupTypes() {
        return groupTypes;
    }

    public void setGroupTypes(List<GroupType> groupTypes) {
        this.groupTypes = groupTypes;
    }

    public List<BillingData> getBilingData() {
        return bilingData;
    }

    public void setBilingData(List<BillingData> bilingData) {
        this.bilingData = bilingData;
    }
}

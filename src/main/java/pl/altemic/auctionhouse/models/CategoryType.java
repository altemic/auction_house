package pl.altemic.auctionhouse.models;

import javax.persistence.*;

/**
 * Created by m.altenberg on 20.12.2016.
 */
@Entity
public class CategoryType extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_categorytype")
    long id;
    String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

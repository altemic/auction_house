package pl.altemic.auctionhouse.models;

import javax.persistence.*;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
public class BillingData extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_billingdata")
    long id;
    String street;
    String postalCode;
    String city;
    String accountNumber;
    String taxIdentificationNumber;
    @ManyToOne
    @JoinTable(name="group_billing_data",
            joinColumns = {@JoinColumn(name="billingdata_id", referencedColumnName = "id_billingdata")},
            inverseJoinColumns = {@JoinColumn(name="group_id", referencedColumnName = "id_group")})
    Group group;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTaxIdentificationNumber() {
        return taxIdentificationNumber;
    }

    public void setTaxIdentificationNumber(String taxIdentificationNumber) {
        this.taxIdentificationNumber = taxIdentificationNumber;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}

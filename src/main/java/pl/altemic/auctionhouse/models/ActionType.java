package pl.altemic.auctionhouse.models;

/**
 * Created by michal on 08.11.2016.
 */
public class ActionType {
    public static int VIEW = 1;
    public static int ADD = 2;
    public static int UPDATE = 3;
    public static int REMOVE = 4;
}

package pl.altemic.auctionhouse.models;

import org.dom4j.tree.BaseElement;
import pl.altemic.auctionhouse.utils.Identity;

import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

/**
 * Created by michal on 13.11.2016.
 */

@MappedSuperclass
public abstract class BaseEntity implements Identity{
    Timestamp timeCreated;
    Timestamp timeModified;
    boolean deleted;

    public Timestamp getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Timestamp timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Timestamp getTimeModified() {
        return timeModified;
    }

    public void setTimeModified(Timestamp timeModified) {
        this.timeModified = timeModified;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}

package pl.altemic.auctionhouse.models;

/**
 * Created by michal on 08.11.2016.
 */

import javax.persistence.*;
import java.util.List;

@Entity(name="grouptype")
public class GroupType extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_grouptype")
    long id;
    @Column(unique = true)
    String name;
    @ManyToMany
    @JoinTable(name="grouptype_roles",
            joinColumns = {@JoinColumn(name="grouptype_id", referencedColumnName = "id_grouptype")},
            inverseJoinColumns = {@JoinColumn(name="role_id", referencedColumnName = "id_role")})
    List<Role> roleList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }
}

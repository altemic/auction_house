package pl.altemic.auctionhouse.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by michal on 13.11.2016.
 */
@Entity
public class Cart extends BaseEntity{
    @Id
    @Column(name = "id_cart")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    @OneToMany
    @JoinColumn(name = "cart_id")
    List<Offer> offerList;
    @OneToOne
    User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Offer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<Offer> offerList) {
        this.offerList = offerList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

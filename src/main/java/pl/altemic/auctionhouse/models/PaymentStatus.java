package pl.altemic.auctionhouse.models;

/**
 * Created by michal on 08.11.2016.
 */
import javax.persistence.*;

@Entity
public class PaymentStatus extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_paymentstatus")
    long id;
    String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package pl.altemic.auctionhouse.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
@DiscriminatorValue("USER")
@PrimaryKeyJoinColumn(name = "id_user", referencedColumnName = "owner_id")
public class User extends Owner{
    @Column(unique = true)
    String username;
    String firstname;
    String lastname;
    String email;
    String password;
    @ManyToOne
    @JoinTable(name="group_user",
            joinColumns = {@JoinColumn(name="user_id", referencedColumnName = "id_user")},
            inverseJoinColumns = {@JoinColumn(name="group_id", referencedColumnName = "id_group")})
    Group group;
    @ManyToMany
    @JoinTable(name="user_roles_custom",
            joinColumns = {@JoinColumn(name="user_id", referencedColumnName = "id_user")},
            inverseJoinColumns = {@JoinColumn(name="role_id", referencedColumnName = "id_role")})
    List<Role> customRoleList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Role> getCustomRoleList() {
        return customRoleList;
    }

    public void setCustomRoleList(List<Role> customRoleList) {
        this.customRoleList = customRoleList;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

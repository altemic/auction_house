package pl.altemic.auctionhouse.models;

/**
 * Created by michal on 08.11.2016.
 */
import javax.persistence.*;

@Entity
public class Offer extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_offer")
    long id;
    int amount;
    @OneToOne
    @JoinColumn(name="price_id")
    Price price;
    @OneToOne
    @JoinColumn(name="buyer_id")
    User buyer;
    @OneToOne
    @JoinColumn(name = "payment_id")
    Payment payment;
    @OneToOne
    @JoinColumn(name="shipping_id")
    Shipping shipping;
    int offerStatus;
    @ManyToOne
    @JoinColumn(name = "auction_id")
    Auction auction;

    public long getId() { return id; }

    public void setId(long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public int getOfferStatus() { return offerStatus; }

    public void setOfferStatus(int offerStatus) { this.offerStatus = offerStatus; }

    public Auction getAuction() { return auction; }

    public void setAuction(Auction auction) { this.auction = auction; }
}

package pl.altemic.auctionhouse.models;

import javax.persistence.*;

/**
 * Created by michal on 08.11.2016.
 */
@Entity
public class Price extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_price")
    long id;
    @ManyToOne
    @JoinColumn(name = "currency_id")
    Currency currency;
    double amount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}

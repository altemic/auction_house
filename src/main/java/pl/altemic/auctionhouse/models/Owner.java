package pl.altemic.auctionhouse.models;

import javax.persistence.*;

/**
 * Created by m.altenberg on 07.02.2017.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "REFERENCE", discriminatorType = DiscriminatorType.STRING)
public abstract class Owner extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="owner_id")
    long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

package pl.altemic.auctionhouse.dto;

public class Price extends BaseDto {

    Currency currency;
    double amount;

    public Price(Currency _currency, double _amount){
        super();
        this.currency = _currency;
        this.amount = _amount;
    }

    public Price() {
        super();
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}

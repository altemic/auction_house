package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.utils.AuctionHouseException;

/**
 * Created by m.altenberg on 11.02.2017.
 */
public class OwnableException extends AuctionHouseException {

    String error_message;

    public OwnableException(String message){
        this.error_message = message;
    }

    @Override
    public String toString(){
        return "Ownable exception: " + error_message;
    }
}

package pl.altemic.auctionhouse.dto;

/**
 * Created by michal on 08.11.2016.
 */

import java.util.ArrayList;
import java.util.List;


public class Payment extends BaseDto {

    long id;
    List<PaymentStatus> paymentStatuses;

    public Payment(){
        super();
    }

    public List<PaymentStatus> getPaymentStatuses() {
        return paymentStatuses;
    }

    public void setPaymentStatuses(List<PaymentStatus> paymentStatuses) {
        this.paymentStatuses = paymentStatuses;
    }
}

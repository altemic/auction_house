package pl.altemic.auctionhouse.dto;

/**
 * Created by m.altenberg on 20.12.2016.
 */
public class CategoryType extends BaseDto{
    String name;

    public CategoryType(){
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

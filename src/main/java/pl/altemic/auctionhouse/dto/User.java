package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.guardian.RoleCombined;

import java.util.List;

/**
 * Created by michal on 08.11.2016.
 */

public class User extends BaseDto implements Ownable {

    String username;
    String firstname;
    String lastname;
    String password;
    String email;

    @RoleCombined(parent = User.class, child = Group.class)
    Group group;

    @RoleCombined(parent = User.class, child = Role.class)
    List<Role> customRoleList;

    public User(String _username, String _firstName, String _lastname, Group _group){
        super();
        this.username =_username;
        this.firstname = _firstName;
        this.lastname = _lastname;
        this.group = _group;
    }

    public User() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Role> getCustomRoleList() {
        return customRoleList;
    }

    public void setCustomRoleList(List<Role> customRoleList) {
        this.customRoleList = customRoleList;
    }

    public User getOwner() {
        return this;
    }

    public void setOwner(Ownable ownable) throws OwnableException {
        throw new OwnableException("User is owner of itself, cannot be set.");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

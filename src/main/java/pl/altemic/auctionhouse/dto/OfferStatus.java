package pl.altemic.auctionhouse.dto;

/**
 * Created by michal on 13.11.2016.
 */
public class OfferStatus {
    public static int CART = 1;
    public static int ORDERED = 2;
}

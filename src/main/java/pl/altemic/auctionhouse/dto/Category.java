package pl.altemic.auctionhouse.dto;

import java.util.List;

/**
 * Created by m.altenberg on 20.12.2016.
 */
public class Category extends BaseDto{
    List<CategoryType> categoryTypeList;
    List<Title> titleList;
    List<Description> descriptionList;
    String name;
    List<Category> dependingCategoryList;

    public Category(){
        super();
    }

    public List<CategoryType> getCategoryTypeList() {
        return categoryTypeList;
    }

    public void setCategoryTypeList(List<CategoryType> categoryTypeList) {
        this.categoryTypeList = categoryTypeList;
    }

    public List<Title> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<Title> titleList) {
        this.titleList = titleList;
    }

    public List<Description> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(List<Description> descriptionList) {
        this.descriptionList = descriptionList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getDependingCategoryList() {
        return dependingCategoryList;
    }

    public void setDependingCategoryList(List<Category> dependingCategoryList) {
        this.dependingCategoryList = dependingCategoryList;
    }
}

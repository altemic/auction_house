package pl.altemic.auctionhouse.dto;

/**
 * Created by michal on 20.11.2016.
 */
public class Config extends BaseDto{

    String name;
    String value;

    public Config(){
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

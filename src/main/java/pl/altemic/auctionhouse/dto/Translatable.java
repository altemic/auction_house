package pl.altemic.auctionhouse.dto;

public class Translatable extends BaseDto {
    Dictionary dictionary;

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public Translatable(){
        super();
    }
}

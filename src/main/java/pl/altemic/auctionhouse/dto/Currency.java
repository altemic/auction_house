package pl.altemic.auctionhouse.dto;

public class Currency extends BaseDto {

    String symbol;
    double exchange;

    public Currency(String _symbol, double _exchange){
        super();
        this.symbol = _symbol;
        this.exchange = _exchange;
    }

    public Currency(){
        super();
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getExchange() {
        return exchange;
    }

    public void setExchange(double exchange) {
        this.exchange = exchange;
    }
}

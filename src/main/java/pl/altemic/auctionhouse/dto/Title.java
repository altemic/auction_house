package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.utils.Identity;

public class Title extends Translatable implements Ownable {

    String text;
    Ownable owner;

    public Title(Dictionary _dictionary, String _text){
        super();
        this.dictionary = _dictionary;
        this.text = _text;
    }

    public Title() {
        super();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public Ownable getOwner() {
        return owner;
    }

    public void setOwner(Ownable owner) {
        this.owner = owner;
    }
}

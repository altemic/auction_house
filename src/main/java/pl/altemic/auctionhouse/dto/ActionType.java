package pl.altemic.auctionhouse.dto;

/**
 * Created by michal on 08.11.2016.
 */

import pl.altemic.auctionhouse.utils.DbInitializerConfig;

@DbInitializerConfig(createPermissions = false)
public class ActionType {
    public static int VIEW = 1;
    public static int ADD = 2;
    public static int UPDATE = 3;
    public static int REMOVE = 4;
}

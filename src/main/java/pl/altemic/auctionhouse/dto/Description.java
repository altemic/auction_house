package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.guardian.Ownable;

public class Description extends Translatable implements Ownable{

    String text;
    Ownable owner;

    public Description(Dictionary _dict, String _text){
        super();
        this.dictionary = _dict;
        this.text = _text;
    }

    public Description(){
        super();
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public Ownable getOwner() {
        return owner;
    }

    @Override
    public void setOwner(Ownable owner) {
        this.owner = owner;
    }
}

package pl.altemic.auctionhouse.dto;

public class Permission extends BaseDto {

    String name;
    PermissionType type;
    String reference;

    public Permission(String _name, PermissionType _type, String _reference){
        super();
        this.name = _name;
        this.type = _type;
        this.reference = _reference;
    }

    public Permission() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public PermissionType getType() { return type; }

    public void setType(PermissionType type) { this.type = type; }
}

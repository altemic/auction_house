package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.guardian.RoleCombined;

import java.util.ArrayList;
import java.util.List;

public class Role extends BaseDto {

    String name;

    @RoleCombined(parent = Role.class, child = Permission.class)
    List<Permission> permissionList;

    public Role(String _name){
        super();
        this.name = _name;
    }

    public Role() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Permission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }
}

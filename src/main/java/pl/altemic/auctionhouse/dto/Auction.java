package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.guardian.Ownable;

import java.util.ArrayList;
import java.util.List;

public class Auction extends BaseDto implements Ownable<User> {

    List<Title> titleList;
    List<Description> descriptionList;
    User seller;
    List<Offer> offerList;

    List<Shipping> shippingList;

    public Auction(){
        super();
    }

    public List<Description> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(List<Description> descriptionList) {
        this.descriptionList = descriptionList;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public List<Offer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<Offer> offerList) {
        this.offerList = offerList;
    }

    public List<Title> getTitleList() { return titleList; }

    public void setTitleList(List<Title> titleList) { this.titleList = titleList; }

    public List<Shipping> getShippingList() {
        return shippingList;
    }

    public void setShippingList(List<Shipping> shippingList) {
        this.shippingList = shippingList;
    }

    @Override
    public User getOwner() {
        return seller;
    }

    @Override
    public void setOwner(User user) throws OwnableException {
        this.setSeller(user);
    }
}

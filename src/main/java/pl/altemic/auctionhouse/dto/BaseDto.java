package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.utils.DbInitializerConfig;
import pl.altemic.auctionhouse.utils.Identity;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Comparator;

/**
 * Created by michal on 13.11.2016.
 */

@DbInitializerConfig(createPermissions = false)
public class BaseDto implements Identity{
    Timestamp timeCreated;
    Timestamp timeModified;
    boolean deleted;
    long id;

    public BaseDto(){
        this.timeCreated = new Timestamp(Calendar.getInstance().getTime().getTime());
        this.timeModified = new Timestamp(Calendar.getInstance().getTime().getTime());
    }

    public Timestamp getTimeModified() {
        return timeModified;
    }

    public void setTimeModified(Timestamp timeModified) {
        this.timeModified = timeModified;
    }

    public Timestamp getTimeCreated() {
        return timeCreated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object other){
        if(!other.getClass().equals(this.getClass())) return false;
        BaseDto OtherBaseDto = (BaseDto)other;
        if(OtherBaseDto.getId() == 0) return false;
        if(this.getId() == OtherBaseDto.getId()) return true;
        return false;
    }
}

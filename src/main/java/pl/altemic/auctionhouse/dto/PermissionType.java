package pl.altemic.auctionhouse.dto;

/**
 * Created by michal on 19.11.2016.
 */
public enum PermissionType {
    ADD,
    UPDATE,
    REMOVE,
    VIEW
}

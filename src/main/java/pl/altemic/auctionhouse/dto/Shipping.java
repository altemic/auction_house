package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.guardian.Ownable;

import java.util.List;

public class Shipping extends BaseDto implements Ownable<Auction> {

    Price price;
    Auction auction;
    List<Title> titleList;
    List<Description> descriptionList;

    public Shipping(){
        super();
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public List<Title> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<Title> titleList) {
        this.titleList = titleList;
    }

    public List<Description> getDescriptionList() {
        return descriptionList;
    }

    public void setDescriptionList(List<Description> descriptionList) {
        this.descriptionList = descriptionList;
    }

    @Override
    public User getOwner() {
        return auction.getOwner();
    }

    @Override
    public void setOwner(Auction ownable) throws OwnableException {
        this.auction = ownable;
    }
}

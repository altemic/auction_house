package pl.altemic.auctionhouse.dto;

public class PaymentStatus extends BaseDto {

    String description;

    public PaymentStatus(String _description){
        super();
        this.description = _description;
    }

    public PaymentStatus() {
        super();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

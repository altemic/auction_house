package pl.altemic.auctionhouse.dto;

public class Dictionary extends BaseDto {

    String symbol;

    public Dictionary(String _symbol){
        super();
        this.symbol = _symbol;
    }

    public Dictionary(){
        super();
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}

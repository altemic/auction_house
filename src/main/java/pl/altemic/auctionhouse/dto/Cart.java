package pl.altemic.auctionhouse.dto;

import java.util.ArrayList;
import java.util.List;

public class Cart extends BaseDto {

    List<Offer> offerList;
    User user;

    public Cart(User _user){
        super();
        this.user = _user;
    }

    public Cart(){
        super();
    }

    public List<Offer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<Offer> offerList) {
        this.offerList = offerList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

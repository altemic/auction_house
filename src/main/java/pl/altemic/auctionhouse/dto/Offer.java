package pl.altemic.auctionhouse.dto;


import pl.altemic.auctionhouse.guardian.Ownable;

public class Offer extends BaseDto implements Ownable<Auction> {

    int amount;
    Price price;
    User buyer;
    Payment payment;
    Shipping shipping;
    int offerStatus;
    Auction auction;

    public Offer(Auction _auction, User _buyer, Price _price, int _amount, Shipping _shipping){
        super();
        this.auction = _auction;
        this.buyer = _buyer;
        this.price = _price;
        this.amount = _amount;
        this.shipping = _shipping;
    }

    public Offer() {
        super();
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public int getOfferStatus() { return offerStatus; }

    public void setOfferStatus(int offerStatus) { this.offerStatus = offerStatus; }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    @Override
    public User getOwner() {
        return auction.getOwner();
    }

    @Override
    public void setOwner(Auction ownable) throws OwnableException {
        this.auction = ownable;
    }
}

package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.guardian.Ownable;

public class BillingData extends BaseDto implements Ownable<Group> {

    String street;
    String postalCode;
    String city;
    String accountNumber;
    String taxIdentificationNumber;
    Group group;

    public BillingData(String _street, String _postalCode, String _city, String _accountNumber){
        //TODO: Verificator - must have
        super();
        this.street = _street;
        this.postalCode = _postalCode;
        this.city = _city;
        this.accountNumber = _accountNumber;
    }

    public BillingData(){
        super();
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTaxIdentificationNumber() {
        return taxIdentificationNumber;
    }

    public void setTaxIdentificationNumber(String taxIdentificationNumber) {
        this.taxIdentificationNumber = taxIdentificationNumber;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public Group getOwner() {
        return group;
    }

    @Override
    public void setOwner(Group group) throws OwnableException {
        setGroup(group);
    }
}

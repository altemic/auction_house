package pl.altemic.auctionhouse.dto;

import java.util.ArrayList;
import java.util.List;

public class GroupType extends BaseDto {

    String name;
    List<Role> roleList;

    public GroupType(String _name){
        super();
        this.name = _name;
    }

    public GroupType() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }
}

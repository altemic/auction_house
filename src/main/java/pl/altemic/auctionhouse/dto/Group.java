package pl.altemic.auctionhouse.dto;

import pl.altemic.auctionhouse.guardian.GuardianConfig;
import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.guardian.RoleCombined;

import java.util.List;

public class Group extends BaseDto implements Ownable {

    String name;
    @RoleCombined(parent = Group.class, child = GroupType.class)
    @GuardianConfig(key = "name:NORMAL_USER_GROUPTYPE")
    List<GroupType> groupTypes;
    List<BillingData> bilingData;

    public Group(String _name){
        super();
        this.name = _name;
    }

    public Group() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GroupType> getGroupTypes() {
        return groupTypes;
    }

    public void setGroupTypes(List<GroupType> groupTypes) {
        this.groupTypes = groupTypes;
    }

    public List<BillingData> getBilingData() {
        return bilingData;
    }

    public void setBilingData(List<BillingData> bilingData) {
        this.bilingData = bilingData;
    }

    @Override
    public Group getOwner() {
        return this;
    }

    @Override
    public void setOwner(Ownable ownable) throws OwnableException {
        throw new OwnableException("Group is owner of itself, cannot be set.");
    }
}

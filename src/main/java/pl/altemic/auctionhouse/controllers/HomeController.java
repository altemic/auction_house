package pl.altemic.auctionhouse.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.altemic.auctionhouse.utils.DbInitializer;

import javax.annotation.PostConstruct;

/**
 * Created by michal on 08.11.2016.
 */
@Controller
public class HomeController {

    DbInitializer dbInitializer;

    @Autowired
    public void setDbInitializer(DbInitializer dbInitializer) {
        this.dbInitializer = dbInitializer;
    }

    @RequestMapping("/")
    public String home() {
        return "home";
    }

    @PostConstruct
    public void Init(){
        dbInitializer.process();
    }
}

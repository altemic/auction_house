package pl.altemic.auctionhouse.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.xml.ws.RequestWrapper;

/**
 * Created by m.altenberg on 19.01.2017.
 */

@Controller
@RequestMapping("/error")
public class ErrorController {

    @RequestMapping("/error/404")
    public String Error404(){
        return "404";
    }
}

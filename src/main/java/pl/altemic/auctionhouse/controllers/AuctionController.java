package pl.altemic.auctionhouse.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.altemic.auctionhouse.dto.*;
import pl.altemic.auctionhouse.modules.AuctionModule;
import pl.altemic.auctionhouse.modules.DescriptionModule;
import pl.altemic.auctionhouse.modules.DictionaryModule;
import pl.altemic.auctionhouse.modules.TitleModule;
import pl.altemic.auctionhouse.security.ContextUser;
import pl.altemic.auctionhouse.utils.AuctionHouseException;
import pl.altemic.auctionhouse.utils.Logger;
import pl.altemic.auctionhouse.viewmodels.AuctionViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by m.altenberg on 03.03.2017.
 */

@Controller
//@RequestMapping(value = "/auctions")
public class AuctionController {

    AuctionModule auctionModule;
	Logger logger;
    ContextUser contextUser;
    DictionaryModule dictionaryModule;
    TitleModule titleModule;
    DescriptionModule descriptionModule;

    @Autowired
    public void setAuctionModule(AuctionModule auctionModule) {
        this.auctionModule = auctionModule;
    }

    @Autowired
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Autowired
    public void setContextUser(ContextUser contextUser) {
        this.contextUser = contextUser;
    }

    @Autowired
    public void setDictionaryModule(DictionaryModule dictionaryModule) {
        this.dictionaryModule = dictionaryModule;
    }

    @Autowired
    public void setTitleModule(TitleModule titleModule) {
        this.titleModule = titleModule;
    }

    @Autowired
    public void setDescriptionModule(DescriptionModule descriptionModule) {
        this.descriptionModule = descriptionModule;
    }

    @GetMapping("/auctions/{id}")
    public String get(Model model, @PathVariable(required = true) long id, @RequestParam String mode) {
        try {
            Auction auction = auctionModule.findOne(id);
            if (auction != null) {
                //TODO: Implement internationalisation
                AuctionViewModel avm = new AuctionViewModel(auction, "PL");
                if(avm.isReady()) {
                    model.addAttribute("auction", avm);
                } else {
                    return "redirect:/error";
                }
            } else {
                return "redirect:/error/notFoundError";
            }

            if(mode == null || mode.equals("")){
                //view auction in readolny mode
            } else if(mode.equals("new")){
                model.addAttribute("auction", new AuctionViewModel());
                return "add-edit-auction";
            } else if(mode.equals("edit")){
                return "add-edit-auction";
            }

            return "redirect:/error";
        } catch (AuctionHouseException aEx) {
            logger.error("AuctionController.getEdit thrown an exception: " + aEx);
            return "redirect:/error";
        }
    }

    @GetMapping("/auctions")
    public String getEmpty(Model model, @RequestParam String mode) {
        if(mode == null || mode.equals("")){
            List<AuctionViewModel> auctionViewModels = new ArrayList<>();
            //Download data from db
            model.addAttribute("auctions", auctionViewModels);
        } else if(mode.equals("new")){
            model.addAttribute("auction", new AuctionViewModel());
            return "add-edit-auction";
        }

        return "redirect:/error";
    }

    @PostMapping("/auctions")
    public String add(@ModelAttribute AuctionViewModel auction) throws AuctionHouseException {

        if(!auction.isReady()){
            return "redirect:/error";
        }

        Title auctionTitle = new Title();
        //TODO: Implement internationalisation
        Dictionary dictionary = dictionaryModule.findBySymbol("PL");
        if(auction.getTitle()!=null && auction.getTitle() != ""){
            auctionTitle.setText(auction.getDescription());
            auctionTitle.setDictionary(dictionary);
            auctionTitle.setDeleted(false);
            auctionTitle = titleModule.add(auctionTitle);
        }
        Description auctionDescription = new Description();
        if(auction.getDescription()!=null && auction.getDescription()!= ""){
            auctionDescription.setDictionary(dictionary);
            auctionDescription.setText(auction.getDescription());
            auctionDescription.setDeleted(false);
            auctionDescription = descriptionModule.add(auctionDescription);
        }
        Auction auctionDb = new Auction();
        User user = contextUser.getLoggedInUser();
        auctionDb.setOwner(user);
        auctionTitle.setOwner(auctionDb);
        auctionDescription.setOwner(auctionDb);

        List<Title> titleList = new ArrayList<>();
        titleList.add(auctionTitle);
        auctionDb.setTitleList(titleList);
        List<Description> descriptionList = new ArrayList<>();
        descriptionList.add(auctionDescription);
        auctionDb.setDescriptionList(descriptionList);

        auctionDb = auctionModule.add(auctionDb);
        String path = "/auctions/" + auctionDb.getId() + "?mode=edit";
        return "redirect:" + path;
    }

    @GetMapping("/test")
    public String test(){
        return "faq";
    }
}

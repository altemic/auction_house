package pl.altemic.auctionhouse.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.altemic.auctionhouse.dto.BillingData;
import pl.altemic.auctionhouse.dto.Group;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.modules.BillingDataModule;
import pl.altemic.auctionhouse.modules.GroupModule;
import pl.altemic.auctionhouse.modules.UserModule;
import pl.altemic.auctionhouse.modules.tools.PasswordTools;
import pl.altemic.auctionhouse.security.ContextUser;
import pl.altemic.auctionhouse.utils.AuctionHouseException;
import pl.altemic.auctionhouse.utils.Mapper;

import java.util.List;

/**
 * Created by michal on 15.01.2017.
 */
@Controller
public class UserController {

    UserModule userModule;
    GroupModule groupModule;
    BillingDataModule billingDataModule;
    ContextUser contextUser;
    Mapper<User, User> mapper;
    PasswordTools passwordTools;

    @Autowired
    public void setUserModule(UserModule userModule) {
        this.userModule = userModule;
    }

    @Autowired
    public void setGroupModule(GroupModule groupModule) {
        this.groupModule = groupModule;
    }

    @Autowired
    public void setContextUser(ContextUser contextUser) {
        this.contextUser = contextUser;
    }

    @Autowired
    public void setMapper(Mapper<User, User> mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setPasswordTools(PasswordTools passwordTools) {
        this.passwordTools = passwordTools;
    }

    @Autowired
    public void setBillingDataModule(BillingDataModule billingDataModule) {
        this.billingDataModule = billingDataModule;
    }

    @RequestMapping("/signin")
    public String login(){
        return "sign-in";
    }

    @PostMapping("/register")
    public String register(
            @RequestParam(name="username") String username,
            @RequestParam(name="firstname") String firstname,
            @RequestParam(name="email") String email,
            @RequestParam(name="password") String password,
            @RequestParam(name="password2") String password2) throws AuctionHouseException {

        if(!password.equals(password2)){
            //redirect to password error page
        }

        //check if password matches requirements
        if(!passwordTools.matchesRequirements(password)){
            //redirect to password to simple page - verification of password
            //may happen through rest endpoint before form post
        }

        Group group = new Group();
        group.setName(username +"_GROUP");
        group = groupModule.add(group);

        User user = new User();
        user.setUsername(username);
        user.setFirstname(firstname);
        user.setPassword(password);
        user.setGroup(group);

        user = userModule.add(user);

        return "redirect:/";
    }

    @GetMapping("/account")
    public String manageAccount(){
        return "manage-account";
    }

    @GetMapping("/account/userdata")
    public String getUserData(Model model){
        User user = contextUser.getLoggedInUser();
        model.addAttribute("user", user);
        return "/fragments/manage-account/settings :: userdata";
    }

    @PostMapping("/account/userdata")
    public String updateUserData(@ModelAttribute User user) throws AuctionHouseException {
        if(userModule.verify(user).isCorrect()) {
            userModule.update(user);
        }
        return "redirect:/account";
    }

    @GetMapping("/account/billingdata")
    public String getBillingData(Model model){
        User user = contextUser.getLoggedInUser();
        Group group = user.getGroup();
        BillingData billingData = new BillingData();
        if(group!=null){
            List<BillingData> billingDataList = group.getBilingData();
            if(billingDataList.size() > 0){
                billingData = billingDataList.get(0);
            }
        }
        model.addAttribute("billingData", billingData);
        return "/fragments/manage-account/settings :: billingdata";
    }

    @PostMapping("/account/billingdata")
    public String updateBillingData(@ModelAttribute BillingData billingData) throws AuctionHouseException {
        User user = contextUser.getLoggedInUser();
        Group group = user.getGroup();
        if(group == null){
            Group newGroup = new Group();
            newGroup.setName(user.getUsername() +"_GROUP");
            newGroup = groupModule.add(newGroup);
        }
        user = contextUser.getLoggedInUser();
        group = user.getGroup();
        if (group!=null){
            List<BillingData> billingDataList = group.getBilingData();
            if(billingDataList!=null && billingDataList.size() > 0){
                long id = billingDataList.get(0).getId();
                billingData.setId(id);
                //TODO: currently there is support only for one billing data for group
                billingData.setGroup(group);
                billingData = billingDataModule.update(billingData);
            } else {
                billingData.setGroup(group);
                billingData = billingDataModule.add(billingData);
            }
        }

        return "redirect:/account/billingdata";
    }
}

package pl.altemic.auctionhouse.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.altemic.auctionhouse.dto.Role;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.modules.RoleModule;
import pl.altemic.auctionhouse.modules.UserModule;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by m.altenberg on 30.12.2016.
 */


@Service("userDetailsService")
public class SecurityUserServiceImpl implements UserDetailsService {

    UserModule userModule;
    RoleModule roleModule;

    @Autowired
    public void setUserModule(UserModule userModule) {
        this.userModule = userModule;
    }

    @Autowired
    public void setRoleModule(RoleModule roleModule) {
        this.roleModule = roleModule;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User userFromDb = userModule.findByUsername(s);
        UserDetails userDetails = convert(userFromDb);
        return userDetails;
    }

    private UserDetails convert(User user){
        List<GrantedAuthority> authorities = new LinkedList<>();
        for(Role r : roleModule.getRolesForUser(user)){
            authorities.add(convertRoleToGrantedAuthority(r));
        }

        org.springframework.security.core.userdetails.User userDetails
                = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true, true, true, true, authorities);
        return userDetails;
    }

    private GrantedAuthority convertRoleToGrantedAuthority(Role role){
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(role.getName());
        return simpleGrantedAuthority;
    }
}

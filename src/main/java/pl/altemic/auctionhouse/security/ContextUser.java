package pl.altemic.auctionhouse.security;

import pl.altemic.auctionhouse.dto.User;

/**
 * Created by michal on 01.01.2017.
 */
public interface ContextUser {
    User getLoggedInUser();
}

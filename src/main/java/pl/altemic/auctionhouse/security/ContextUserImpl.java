package pl.altemic.auctionhouse.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.modules.UserModule;
import pl.altemic.auctionhouse.utils.Constants;

import javax.annotation.Resource;

/**
 * Created by michal on 01.01.2017.
 */

@Component
public class ContextUserImpl implements ContextUser {

    UserModule userModule;

    @Autowired
    public void setUserModule(UserModule userModule) {
        this.userModule = userModule;
    }

    @Override
    public User getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user;
        if(!(authentication instanceof AnonymousAuthenticationToken) && authentication != null) {
            String username = authentication.getName();
            user = userModule.findByUsername(username);
            if(user != null){
                return user;
            }
        }
        user = userModule.findByUsername(Constants.ANONUMOUS_USERNAME);
        return user;
    }
}

package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.BillingData;

/**
 * Created by michal on 15.11.2016.
 */
public interface BillingDataService extends BaseCRUDService<BillingData> {
}

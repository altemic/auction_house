package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Role;

/**
 * Created by michal on 14.11.2016.
 */
public interface RoleService extends BaseCRUDService<Role>  {
    Role findByName(String name);
}

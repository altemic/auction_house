package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Title;
import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.models.Dictionary;
import pl.altemic.auctionhouse.models.Owner;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.TitleRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;
import pl.altemic.auctionhouse.utils.OwnableConverter;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by michal on 16.11.2016.
 */

@Repository
public class TitleServiceImpl extends BaseCRUDServiceImpl<Title, pl.altemic.auctionhouse.models.Title> implements TitleService,Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Title, pl.altemic.auctionhouse.dto.Title> modelToDtoMapper;
    Mapper<pl.altemic.auctionhouse.dto.Title, pl.altemic.auctionhouse.models.Title> dtoToModelMapper;
    TitleRepository titleRepository;
    ServiceFactory serviceFactory;
    OwnableConverter ownableConverter;
    DictionaryServiceImpl dictionaryServiceImpl;
    IdentityComparator identityComparator;
    ServiceUtils serviceUtils;


    @Autowired
    public void setServiceFactory(@Lazy  ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Autowired
    public void setOwnableConverter(OwnableConverter ownableConverter) {
        this.ownableConverter = ownableConverter;
    }

    @Autowired
    public void setModelToDtoMapper(Mapper<pl.altemic.auctionhouse.models.Title, Title> modelToDtoMapper) {
        this.modelToDtoMapper = modelToDtoMapper;
    }

    @Autowired
    public void setDictionaryServiceImpl(DictionaryServiceImpl dictionaryServiceImpl) {
        this.dictionaryServiceImpl = dictionaryServiceImpl;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public void setServiceUtils(ServiceUtils serviceUtils) {
        this.serviceUtils = serviceUtils;
    }

    @Autowired
    public TitleServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Title, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Title> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Title, Title> _modelToDtoMapper,
            Mapper<Title, pl.altemic.auctionhouse.models.Title> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Title.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.titleRepository = (TitleRepository)_jpaRepo;
    }

    @Override
    public List<Title> findByText(String text) {
        List<pl.altemic.auctionhouse.models.Title> model = titleRepository.findByText(text);
        if(model == null) return null;
        List<Title> dtos = model.stream().map(m -> FromModelToDto(m)).collect(Collectors.toList());
        return dtos;
    }

    @Override
    public List<Title> findByOwner(long id) {
        if(id == 0) return null;
        List<pl.altemic.auctionhouse.models.Title> modelList = titleRepository.findByOwner(id);
        if(modelList == null || modelList.size() == 0) return null;
        List<Title> dtoList = modelList.stream().map(t -> FromModelToDto(t)).collect(Collectors.toList());
        return dtoList;
    }

    @Override
    public Title FromModelToDto(pl.altemic.auctionhouse.models.Title model, Object... args) {
        if(model == null) return null;
        Title dto = new Title();
        modelToDtoMapper.Convert(model, dto);
        if(model.getDictionary() != null){
            dto.setDictionary(dictionaryServiceImpl.FromModelToDto(model.getDictionary()));
        }
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Title FromDtoToModel(Title dto, Object... args) {
        pl.altemic.auctionhouse.models.Title model = titleRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Title();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getOwner()!=null && !identityComparator.areTheSame(model.getOwner(), dto.getOwner())){
            try {
                boolean assigned = false;
                if(args!=null
                        && args.length > 0
                        && Map.class.isAssignableFrom(args[0].getClass())){
                    Map<String, Object> alreadyMapped = (Map<String, Object>)args[0];
                    Object foundMapped = serviceUtils.getAlreadyMapped(alreadyMapped, Owner.class, "");
                    if(foundMapped != null){
                        Owner owner = (Owner)foundMapped;
                        model.setOwner(owner);
                        assigned = true;
                    }
//                    serviceUtils.rewriteWhatPossible(model, alreadyMapped);
                }

                if(!assigned) {
                    Owner owner = ownableConverter.FromDtoToModel(dto.getOwner());
                    model.setOwner(owner);
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }




        }
        if(dto.getDictionary()!=null && !identityComparator.areTheSame(model.getDictionary(), dto.getDictionary())){
            Dictionary dict = dictionaryServiceImpl.FromDtoToModel(dto.getDictionary());
            model.setDictionary(dict);
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Title dto = (Title)target;
        pl.altemic.auctionhouse.models.Title model = titleRepository.findOne(dto.getId());
        if(model.getOwner()!=null){
            try {
                Ownable owner = ownableConverter.FromModelToDto(model.getOwner());
                dto.setOwner(owner);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return dto;
    }
}

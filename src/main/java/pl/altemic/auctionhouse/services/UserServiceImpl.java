package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Role;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.guardian.PermissionType;
import pl.altemic.auctionhouse.optimizer.LazyRemover;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.UserRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
public class UserServiceImpl extends BaseCRUDServiceImpl<User, pl.altemic.auctionhouse.models.User> implements UserService,Optimizer {

    Mapper<pl.altemic.auctionhouse.models.User, User> modelToDtoMapper;
    Mapper<User, pl.altemic.auctionhouse.models.User> dtoToModelMapper;
    UserRepository userRepository;
    GroupServiceImpl groupServiceImpl;
    RoleServiceImpl roleServiceImpl;
    IdentityComparator identityComparator;

    @Autowired
    public void setRoleServiceImpl(RoleServiceImpl roleServiceImpl) {
        this.roleServiceImpl = roleServiceImpl;
    }

    @Autowired
    public void setGroupServiceImpl(@Lazy GroupServiceImpl groupServiceImpl) {
        this.groupServiceImpl = groupServiceImpl;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public UserServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.User, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.User> wrapper,
            Mapper<pl.altemic.auctionhouse.models.User, User> _modelToDtoMapper,
            Mapper<User, pl.altemic.auctionhouse.models.User> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), User.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.userRepository = (UserRepository)_jpaRepo;
    }

    @Override
    @PermissionType(permissionType = pl.altemic.auctionhouse.dto.PermissionType.VIEW)
    public User findOne(Long id) {
        pl.altemic.auctionhouse.models.User model = baseCRUDService.findOne(id);
        User dto = FromModelToDto(model);
        return dto;
    }

    @Override
    public User FromModelToDto(pl.altemic.auctionhouse.models.User model, Object... args) {
        if(model == null) return null;
        User dto = new User();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.User FromDtoToModel(User dto, Object... args) {
        pl.altemic.auctionhouse.models.User model = userRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.User();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getCustomRoleList()!=null && !identityComparator.areTheSame(model.getCustomRoleList(), dto.getCustomRoleList())){
            List<pl.altemic.auctionhouse.models.Role> roleList = dto.getCustomRoleList().stream().map(r -> roleServiceImpl.FromDtoToModel(r)).collect(Collectors.toList());
            model.setCustomRoleList(roleList);
        }
        if(dto.getGroup()!=null && !identityComparator.areTheSame(model.getGroup(), dto.getGroup())){
            model.setGroup(groupServiceImpl.FromDtoToModel(dto.getGroup()));
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public User findByUsername(String username) {
        pl.altemic.auctionhouse.models.User model = userRepository.findByUsername(username);
        if(model == null) return null;
        User dto = FromModelToDto(model);
        return dto;
    }

    @Override
    public Object LoadAllFields(Object target) {
        User source = (User)target;
        pl.altemic.auctionhouse.models.User dbUser = userRepository.findOne(source.getId());
        if(dbUser.getCustomRoleList()!=null){
            List<Role> roleList = dbUser.getCustomRoleList().stream().map(r -> roleServiceImpl.FromModelToDto(r)).collect(Collectors.toList());
            source.setCustomRoleList(roleList);
        }
        if(dbUser.getGroup()!=null){
            source.setGroup(groupServiceImpl.FromModelToDto(dbUser.getGroup()));
        }
        return source;
    }
}

package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.User;

/**
 * Created by michal on 15.11.2016.
 */
public interface UserService extends BaseCRUDService<User> {
    User findByUsername(String username);
}

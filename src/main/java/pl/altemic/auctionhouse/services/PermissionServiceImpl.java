package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.ActionType;
import pl.altemic.auctionhouse.dto.Permission;
import pl.altemic.auctionhouse.dto.PermissionType;
import pl.altemic.auctionhouse.guardian.GuardianConfig;
import pl.altemic.auctionhouse.repositories.PermissionRepository;
import pl.altemic.auctionhouse.utils.Mapper;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 13.11.2016.
 */

@Repository
public class PermissionServiceImpl extends BaseCRUDServiceImpl<Permission, pl.altemic.auctionhouse.models.Permission> implements PermissionService {

    Mapper<pl.altemic.auctionhouse.models.Permission, Permission> modelToDtoMapper;
    Mapper<Permission, pl.altemic.auctionhouse.models.Permission> dtoToModelMapper;
    PermissionRepository permissionRepository;

    @Autowired
    public PermissionServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Permission, Long> _permissionJpa,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Permission> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Permission, Permission> _modelToDtoMapper,
            Mapper<Permission, pl.altemic.auctionhouse.models.Permission> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_permissionJpa), Permission.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.permissionRepository = (PermissionRepository) _permissionJpa;
    }

    @Override
    public Permission FromModelToDto(pl.altemic.auctionhouse.models.Permission model, Object... args) {
        if(model == null) return null;
        Permission permissionDto = new Permission();
        modelToDtoMapper.Convert(model, permissionDto);
        PermissionType type = getPermissionTypeFromInt(model.getType());
        permissionDto.setType(type);
        return permissionDto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Permission FromDtoToModel(Permission dto, Object... args) {
        pl.altemic.auctionhouse.models.Permission permissionModel = permissionRepository.findOne(dto.getId());
        if(permissionModel == null){
            permissionModel = new pl.altemic.auctionhouse.models.Permission();
        }
        dtoToModelMapper.Convert(dto, permissionModel);
        int action = getActionTypeFromPermissionType(dto.getType().toString());
        permissionModel.setType(action);
        permissionModel.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(permissionModel.getTimeCreated() == null){
            permissionModel.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return permissionModel;
    }

    @Override
    public Permission findByNameAndReference(String name, String reference) {
        pl.altemic.auctionhouse.models.Permission permission = permissionRepository.findByNameAndReference(name, reference);
        if(permission==null) return null;
        return FromModelToDto(permission);
    }

    @Override
    public Permission findByName(String name) {
        pl.altemic.auctionhouse.models.Permission permission = permissionRepository.findByName(name);
        if(permission==null) return null;
        return FromModelToDto(permission);
    }

    @GuardianConfig(NotControlled = true)
    public Permission findByTypeAndReference(String type, String reference){
        Integer typeInt = getActionTypeFromPermissionType(type);
        pl.altemic.auctionhouse.models.Permission permission = permissionRepository.findByTypeAndReference(typeInt, reference);
        if(permission==null) return null;
        return FromModelToDto(permission);
    }

    private int getActionTypeFromPermissionType(String type){
        String typeName = type.toString();
        Field[] fields = ActionType.class.getDeclaredFields();
        List<Field> fieldList = Arrays.asList(fields).stream().filter(f -> f.getName().equals(typeName)).collect(Collectors.toList());
        Field field = fieldList.get(0);
        int actionType = pl.altemic.auctionhouse.models.ActionType.VIEW;
        try {
            actionType = field.getInt(new ActionType());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return actionType;
    }

    private PermissionType getPermissionTypeFromInt(int actionType) {
        Field[] fields = ActionType.class.getDeclaredFields();
        for(Field field : fields){
            try {
                if(field.getInt(new pl.altemic.auctionhouse.models.ActionType()) == actionType){
                    return Enum.valueOf(PermissionType.class, field.getName());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return PermissionType.VIEW;
    }
}

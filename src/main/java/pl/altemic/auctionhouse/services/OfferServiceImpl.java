package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Auction;
import pl.altemic.auctionhouse.dto.Offer;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.OfferRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by michal on 15.11.2016.
 */
@Repository
public class OfferServiceImpl extends BaseCRUDServiceImpl<Offer, pl.altemic.auctionhouse.models.Offer> implements OfferService, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Offer, Offer> modelToDtoMapper;
    Mapper<Offer, pl.altemic.auctionhouse.models.Offer> dtoToModelMapper;
    PriceServiceImpl priceServiceImpl;
    UserServiceImpl userServiceImpl;
    PaymentServiceImpl paymentServiceImpl;
    ShippingServiceImpl shippingServiceImpl;
    OfferRepository offerRepository;
    IdentityComparator identityComparator;
    AuctionServiceImpl auctionServiceImpl;
    ServiceUtils serviceUtils;

    @Autowired
    public void setPriceServiceImpl(PriceServiceImpl priceServiceImpl) {
        this.priceServiceImpl = priceServiceImpl;
    }

    @Autowired
    public void setUserServiceImpl(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @Autowired
    public void setPaymentServiceImpl(PaymentServiceImpl paymentServiceImpl) {
        this.paymentServiceImpl = paymentServiceImpl;
    }

    @Autowired
    public void setShippingServiceImpl(ShippingServiceImpl shippingServiceImpl) {
        this.shippingServiceImpl = shippingServiceImpl;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public void setAuctionServiceImpl(AuctionServiceImpl auctionServiceImpl) {
        this.auctionServiceImpl = auctionServiceImpl;
    }

    @Autowired
    public void setModelToDtoMapper(Mapper<pl.altemic.auctionhouse.models.Offer, Offer> modelToDtoMapper) {
        this.modelToDtoMapper = modelToDtoMapper;
    }

    @Autowired
    public OfferServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Offer, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Offer> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Offer, Offer> _modelToDtoMapper,
            Mapper<Offer, pl.altemic.auctionhouse.models.Offer> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Offer.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.offerRepository = (OfferRepository)_jpaRepo;
    }

    @Override
    public Offer FromModelToDto(pl.altemic.auctionhouse.models.Offer model, Object... args) {
        if(model == null) return null;
        Offer dto = new Offer();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Offer FromDtoToModel(Offer dto, Object... args) {
        pl.altemic.auctionhouse.models.Offer model = offerRepository.findOne(dto.getId());
        dtoToModelMapper.Convert(dto, model);

        boolean passedAuction = false;
        if(args!=null
                && args.length > 0
                && Map.class.isAssignableFrom(args[0].getClass())) {
            Map<String, Object> alreadyMapped = (Map<String, Object>)args[0];
            Object foundMapped = serviceUtils.getAlreadyMapped(alreadyMapped, Auction.class, "");
            if(foundMapped != null){
                model.setAuction((pl.altemic.auctionhouse.models.Auction) foundMapped);
                passedAuction = true;
            }
        }

        if(!passedAuction && dto.getAuction()!=null && ! !identityComparator.areTheSame(model.getAuction(), dto.getAuction())){
            model.setAuction(auctionServiceImpl.FromDtoToModel(dto.getAuction()));
        }

        if(dto.getPrice()!=null && !identityComparator.areTheSame(dto.getPrice(), model.getPrice())){
            model.setPrice(priceServiceImpl.FromDtoToModel(dto.getPrice()));
        }
        if(dto.getBuyer()!=null && !identityComparator.areTheSame(model.getBuyer(), dto.getBuyer())){
            model.setBuyer(userServiceImpl.FromDtoToModel(dto.getBuyer()));
        }
        if(dto.getPayment()!=null && !identityComparator.areTheSame(model.getPayment(), dto.getPayment())){
            model.setPayment(paymentServiceImpl.FromDtoToModel(dto.getPayment()));
        }
        if(dto.getShipping()!=null && !identityComparator.areTheSame(model.getShipping(), dto.getShipping())){
            model.setShipping(shippingServiceImpl.FromDtoToModel(dto.getShipping()));
        }

        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Offer dto = (Offer)target;
        pl.altemic.auctionhouse.models.Offer model = offerRepository.findOne(dto.getId());
        if(model == null) return null;
        modelToDtoMapper.Convert(model, dto);
        if(model.getPrice()!=null){
            dto.setPrice(priceServiceImpl.FromModelToDto(model.getPrice()));
        }
        if(model.getBuyer()!=null){
            dto.setBuyer(userServiceImpl.FromModelToDto(model.getBuyer()));
        }
        if(model.getPayment()!=null){
            dto.setPayment(paymentServiceImpl.FromModelToDto(model.getPayment()));
        }
        if(model.getShipping()!=null){
            dto.setShipping(shippingServiceImpl.FromModelToDto(model.getShipping()));
        }
        if(model.getAuction()!=null){
            pl.altemic.auctionhouse.models.Auction auctionModel = model.getAuction();
            Auction auctionDto = new Auction();
            auctionDto.setId(auctionModel.getId());
            dto.setAuction(auctionDto);
        }
        return dto;
    }
}

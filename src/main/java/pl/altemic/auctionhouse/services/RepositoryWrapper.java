package pl.altemic.auctionhouse.services;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by michal on 14.11.2016.
 */
public interface RepositoryWrapper<T> {
    BaseCRUDService<T> JpaRepositoryWrapper(JpaRepository<T, Long> jpaRepository);
}

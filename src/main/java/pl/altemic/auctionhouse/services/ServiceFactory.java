package pl.altemic.auctionhouse.services;

/**
 * Created by michal on 12.12.2016.
 */
public interface ServiceFactory {
    public <T> T getService(String entityName) throws ClassNotFoundException;
}

package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Dictionary;

/**
 * Created by michal on 15.11.2016.
 */
public interface DictionaryService extends BaseCRUDService<Dictionary> {
    Dictionary findBySymbol(String symbol);
}

package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Price;

/**
 * Created by michal on 15.11.2016.
 */
public interface PriceService extends BaseCRUDService<Price> {
}

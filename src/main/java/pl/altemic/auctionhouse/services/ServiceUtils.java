package pl.altemic.auctionhouse.services;

import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by michal on 23.04.2017.
 */
@Component
public class ServiceUtils {

    public Object getAlreadyMapped(Map<String, Object> alreadyMapped, Class lookFor, String fieldName) {
        if(alreadyMapped==null || alreadyMapped.size() == 0 || lookFor == null)
            return null;

        //Search by type only
        if(fieldName == null || fieldName.equals("")){
            for(Object o : alreadyMapped.values()){
                if(lookFor.isAssignableFrom(o.getClass())) return o;
            }
            return null;
        }

        for(Map.Entry<String, Object> record: alreadyMapped.entrySet()){
            if(lookFor.isAssignableFrom(record.getValue().getClass()) && record.getKey().equals(fieldName))
                return record.getValue();
        }

        return null;
    }

    public Set<String> rewriteWhatPossible(Object dto, Map<String, Object> fieldsPassed){
        Set<String> rewrittenFields = new HashSet<>();
        for(Field f : dto.getClass().getDeclaredFields()){
            Object value = getAlreadyMapped(fieldsPassed, f.getDeclaringClass(), f.getName());
            try {
                Field field = dto.getClass().getDeclaredField(f.getName());
                field.setAccessible(true);
                field.set(dto, value);
                rewrittenFields.add(f.getName());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        return rewrittenFields;
    }
}

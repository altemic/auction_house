package pl.altemic.auctionhouse.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.CategoryType;
import pl.altemic.auctionhouse.repositories.CategoryTypeRepository;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by m.altenberg on 20.12.2016.
 */
@Repository
public class CategoryTypeServiceImpl extends BaseCRUDServiceImpl<CategoryType, pl.altemic.auctionhouse.models.CategoryType> implements CategoryTypeService {

    Mapper<pl.altemic.auctionhouse.models.CategoryType, CategoryType> modelToDtoMapper;
    Mapper<CategoryType, pl.altemic.auctionhouse.models.CategoryType> dtoToModelMapper;
    CategoryTypeRepository categoryTypeRepository;

    @Autowired
    public CategoryTypeServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.CategoryType, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.CategoryType> wrapper,
            Mapper<pl.altemic.auctionhouse.models.CategoryType, CategoryType> _modelToDtoMapper,
            Mapper<CategoryType, pl.altemic.auctionhouse.models.CategoryType> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), CategoryType.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.categoryTypeRepository = (CategoryTypeRepository)_jpaRepo;
    }

    @Override
    public CategoryType FromModelToDto(pl.altemic.auctionhouse.models.CategoryType model, Object... args) {
        if(model == null) return null;
        CategoryType dto = new CategoryType();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.CategoryType FromDtoToModel(CategoryType dto, Object... args) {
        pl.altemic.auctionhouse.models.CategoryType model = categoryTypeRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.CategoryType();
        }
        dtoToModelMapper.Convert(dto, model);
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public CategoryType findByName(String name) {
        pl.altemic.auctionhouse.models.CategoryType categoryTypeModel = categoryTypeRepository.findByName(name);
        if(categoryTypeModel==null) return null;
        CategoryType categoryType = FromModelToDto(categoryTypeModel);
        return categoryType;
    }
}

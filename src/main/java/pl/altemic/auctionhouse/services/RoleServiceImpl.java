package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Role;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.RoleRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.stream.Collectors;

/**
 * Created by michal on 14.11.2016.
 */
@Repository
public class RoleServiceImpl extends BaseCRUDServiceImpl<Role, pl.altemic.auctionhouse.models.Role> implements RoleService, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Role, Role> modelToDtoMapper;
    Mapper<Role, pl.altemic.auctionhouse.models.Role> dtoToModelMapper;
    RoleRepository roleRepository;
    PermissionServiceImpl permissionServiceImpl;
    IdentityComparator identityComparator;

    @Autowired
    public void setPermissionServiceImpl(PermissionServiceImpl permissionServiceImpl) {
        this.permissionServiceImpl = permissionServiceImpl;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public RoleServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Role, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Role> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Role, Role> _modelToDtoMapper,
            Mapper<Role, pl.altemic.auctionhouse.models.Role> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Role.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.roleRepository = (RoleRepository) _jpaRepo;
    }

    @Override
    public Role FromModelToDto(pl.altemic.auctionhouse.models.Role model, Object... args) {
        if(model == null) return null;
        Role roleDto = new Role();
        modelToDtoMapper.Convert(model, roleDto);
        return roleDto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Role FromDtoToModel(Role dto, Object... args) {
        pl.altemic.auctionhouse.models.Role model = roleRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Role();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getPermissionList()!=null && !identityComparator.areTheSame(model.getPermissionList(), dto.getPermissionList())) {
            model.setPermissionList(dto.getPermissionList().stream()
                    .map(p -> permissionServiceImpl.FromDtoToModel(p))
                    .collect(Collectors.toList()));
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Role findByName(String name) {
        pl.altemic.auctionhouse.models.Role model = roleRepository.findByName(name);
        if(model == null) return null;
        Role dto = FromModelToDto(model);
        return dto;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Role dto = (Role)target;
        pl.altemic.auctionhouse.models.Role model = roleRepository.findOne(dto.getId());
        if(model == null) return null;
        modelToDtoMapper.Convert(model, dto);
        if(model.getPermissionList()!=null) {
            dto.setPermissionList(model.getPermissionList().stream()
                    .map(p -> permissionServiceImpl.FromModelToDto(p))
                    .collect(Collectors.toList()));
        }
        return dto;
    }
}


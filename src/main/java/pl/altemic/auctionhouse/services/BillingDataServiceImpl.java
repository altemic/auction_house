package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.BillingData;
import pl.altemic.auctionhouse.dto.Group;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.BillingDataRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by michal on 15.11.2016.
 */
@Repository
public class BillingDataServiceImpl extends BaseCRUDServiceImpl<BillingData, pl.altemic.auctionhouse.models.BillingData> implements BillingDataService, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.BillingData, BillingData> modelToDtoMapper;
    Mapper<BillingData, pl.altemic.auctionhouse.models.BillingData> dtoToModelMapper;
    GroupServiceImpl groupServiceImpl;
    BillingDataRepository billingDataRepository;
    IdentityComparator identityComparator;

    @Autowired
    public void setGroupServiceImpl(@Lazy  GroupServiceImpl groupServiceImpl) {
        this.groupServiceImpl = groupServiceImpl;
    }

    @Autowired
    public void setModelToDtoMapper(Mapper<pl.altemic.auctionhouse.models.BillingData, BillingData> modelToDtoMapper) {
        this.modelToDtoMapper = modelToDtoMapper;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public BillingDataServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.BillingData, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.BillingData> wrapper,
            Mapper<pl.altemic.auctionhouse.models.BillingData, BillingData> _modelToDtoMapper,
            Mapper<BillingData, pl.altemic.auctionhouse.models.BillingData> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), BillingData.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.billingDataRepository = (BillingDataRepository) _jpaRepo;
    }

    @Override
    public BillingData FromModelToDto(pl.altemic.auctionhouse.models.BillingData model, Object... args) {
        if(model == null) return null;
        BillingData dto = new BillingData();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.BillingData FromDtoToModel(BillingData dto, Object... args) {
        pl.altemic.auctionhouse.models.BillingData model = billingDataRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.BillingData();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getGroup()!=null && !identityComparator.areTheSame(dto.getGroup(), model.getGroup())){
            pl.altemic.auctionhouse.models.Group group = groupServiceImpl.FromDtoToModel(dto.getGroup());
            model.setGroup(group);
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Object LoadAllFields(Object target) {
        BillingData dto = (BillingData)target;
        pl.altemic.auctionhouse.models.BillingData model = billingDataRepository.findOne(dto.getId());
        if(model.getGroup()!=null){
            Group group = groupServiceImpl.FromModelToDto(model.getGroup());
            dto.setGroup(group);
        }
        return dto;
    }
}

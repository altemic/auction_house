package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Auction;

/**
 * Created by michal on 15.11.2016.
 */
public interface AuctionService extends BaseCRUDService<Auction> {
}

package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.BillingData;
import pl.altemic.auctionhouse.dto.Group;
import pl.altemic.auctionhouse.dto.GroupType;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.GroupRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
public class GroupServiceImpl extends BaseCRUDServiceImpl<Group, pl.altemic.auctionhouse.models.Group> implements GroupService, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Group, Group> modelToDtoMapper;
    Mapper<Group, pl.altemic.auctionhouse.models.Group> dtoToModelMapper;
    GroupTypeServiceImpl groupTypeServiceImpl;
    BillingDataServiceImpl billingDataServiceImpl;
    GroupRepository groupRepository;
    IdentityComparator identityComparator;

    @Autowired
    public void setGroupTypeServiceImpl(GroupTypeServiceImpl groupTypeServiceImpl) {
        this.groupTypeServiceImpl = groupTypeServiceImpl;
    }

    @Autowired
    public void setBillingDataServiceImpl(BillingDataServiceImpl billingDataServiceImpl) {
        this.billingDataServiceImpl = billingDataServiceImpl;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public GroupServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Group, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Group> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Group, Group> _modelToDtoMapper,
            Mapper<Group, pl.altemic.auctionhouse.models.Group> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Group.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.groupRepository = (GroupRepository)_jpaRepo;
    }

    @Override
    public Group FromModelToDto(pl.altemic.auctionhouse.models.Group model, Object... args) {
        if(model == null) return null;
        Group dto = new Group();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Group FromDtoToModel(Group dto, Object... args) {
        pl.altemic.auctionhouse.models.Group model = groupRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Group();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getGroupTypes()!=null && !identityComparator.areTheSame(model.getGroupTypes(), dto.getGroupTypes())){
            List<pl.altemic.auctionhouse.models.GroupType> groupTypeList = dto.getGroupTypes().stream().map(g -> groupTypeServiceImpl.FromDtoToModel(g)).collect(Collectors.toList());
            model.setGroupTypes(groupTypeList);
        }
        if(dto.getBilingData()!=null && !identityComparator.areTheSame(model.getBilingData(), dto.getBilingData())){
            List<pl.altemic.auctionhouse.models.BillingData> billingDataList = dto.getBilingData().stream().map(b -> billingDataServiceImpl.FromDtoToModel(b)).collect(Collectors.toList());
            model.setBilingData(billingDataList);
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Group findByName(String name) {
        pl.altemic.auctionhouse.models.Group group = groupRepository.findByName(name);
        if(group==null) return null;
        return FromModelToDto(group);
    }

    @Override
    public Object LoadAllFields(Object target) {
        Group dto = (Group)target;
        pl.altemic.auctionhouse.models.Group model = groupRepository.findOne(dto.getId());
        if(model.getGroupTypes()!=null){
            List<GroupType> groupTypeList = model.getGroupTypes().stream().map(g -> groupTypeServiceImpl.FromModelToDto(g)).collect(Collectors.toList());
            dto.setGroupTypes(groupTypeList);
        }
        if(model.getBilingData()!=null){
            List<BillingData> billingDataList = model.getBilingData().stream().map(b -> billingDataServiceImpl.FromModelToDto(b)).collect(Collectors.toList());
            dto.setBilingData(billingDataList);
        }
        return dto;
    }
}

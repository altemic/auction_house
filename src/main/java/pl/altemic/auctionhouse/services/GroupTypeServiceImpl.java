package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.GroupType;
import pl.altemic.auctionhouse.dto.Role;
import pl.altemic.auctionhouse.guardian.GuardianConfig;
import pl.altemic.auctionhouse.repositories.GroupTypeRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
public class GroupTypeServiceImpl extends BaseCRUDServiceImpl<GroupType, pl.altemic.auctionhouse.models.GroupType> implements GroupTypeService {

    Mapper<pl.altemic.auctionhouse.models.GroupType, GroupType> modelToDtoMapper;
    Mapper<GroupType, pl.altemic.auctionhouse.models.GroupType> dtoToModelMapper;
    RoleServiceImpl roleServiceImpl;
    GroupTypeRepository groupTypeRepository;
    IdentityComparator identityComparator;

    @Autowired
    public void setRoleServiceImpl(RoleServiceImpl roleServiceImpl) {
        this.roleServiceImpl = roleServiceImpl;
    }

    @Autowired
    public void setModelToDtoMapper(Mapper<pl.altemic.auctionhouse.models.GroupType, GroupType> modelToDtoMapper) {
        this.modelToDtoMapper = modelToDtoMapper;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public GroupTypeServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.GroupType, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.GroupType> wrapper,
            Mapper<pl.altemic.auctionhouse.models.GroupType, GroupType> _modelToDtoMapper,
            Mapper<GroupType, pl.altemic.auctionhouse.models.GroupType> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), GroupType.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.groupTypeRepository = (GroupTypeRepository)_jpaRepo;
    }

    @Override
    public GroupType FromModelToDto(pl.altemic.auctionhouse.models.GroupType model, Object... args) {
        if(model == null) return null;
        GroupType dto = new GroupType();
        modelToDtoMapper.Convert(model, dto);
        if(model.getRoleList()!=null){
            List<Role> roleList = model.getRoleList().stream().map(r -> roleServiceImpl.FromModelToDto(r)).collect(Collectors.toList());
            dto.setRoleList(roleList);
        }
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.GroupType FromDtoToModel(GroupType dto, Object... args) {
        pl.altemic.auctionhouse.models.GroupType model = groupTypeRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.GroupType();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getRoleList()!=null && !identityComparator.areTheSame(dto.getRoleList(), model.getRoleList())){
            List<pl.altemic.auctionhouse.models.Role> roleList = dto.getRoleList().stream().map(r -> roleServiceImpl.FromDtoToModel(r)).collect(Collectors.toList());
            model.setRoleList(roleList);
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    @GuardianConfig(finderParameter = "name:arg0")
    public GroupType findByName(String name) {
        pl.altemic.auctionhouse.models.GroupType model = groupTypeRepository.findByName(name);
        if(model==null) return null;
        GroupType dto = FromModelToDto(model);
        return dto;
    }
}

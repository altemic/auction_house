package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Auction;
import pl.altemic.auctionhouse.dto.Description;
import pl.altemic.auctionhouse.dto.Shipping;
import pl.altemic.auctionhouse.dto.Title;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.ShippingRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
public class ShippingServiceImpl extends BaseCRUDServiceImpl<Shipping, pl.altemic.auctionhouse.models.Shipping> implements BaseCRUDService<Shipping>, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Shipping, Shipping> modelToDtoMapper;
    Mapper<Shipping, pl.altemic.auctionhouse.models.Shipping> dtoToModelMapper;
    PriceServiceImpl priceServiceImpl;
    AuctionServiceImpl auctionServiceImpl;
    ShippingRepository shippingRepository;
    TitleServiceImpl titleServiceImpl;
    DescriptionServiceImpl descriptionServiceImpl;
    IdentityComparator identityComparator;
    ServiceUtils serviceUtils;

    @Autowired
    public void setPriceServiceImpl(PriceServiceImpl priceServiceImpl) {
        this.priceServiceImpl = priceServiceImpl;
    }

    @Autowired
    public void setAuctionServiceImpl(@Lazy  AuctionServiceImpl auctionServiceImpl) {
        this.auctionServiceImpl = auctionServiceImpl;
    }

    @Autowired
    public void setTitleServiceImpl(@Lazy  TitleServiceImpl titleServiceImpl) {
        this.titleServiceImpl = titleServiceImpl;
    }

    @Autowired
    public void setDescriptionServiceImpl(@Lazy DescriptionServiceImpl descriptionServiceImpl) {
        this.descriptionServiceImpl = descriptionServiceImpl;
    }

    @Autowired
    public void setModelToDtoMapper(Mapper<pl.altemic.auctionhouse.models.Shipping, Shipping> modelToDtoMapper) {
        this.modelToDtoMapper = modelToDtoMapper;
    }

    @Autowired
    public void setServiceUtils(ServiceUtils serviceUtils) {
        this.serviceUtils = serviceUtils;
    }

    @Autowired
    public ShippingServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Shipping, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Shipping> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Shipping, Shipping> _modelToDtoMapper,
            Mapper<Shipping, pl.altemic.auctionhouse.models.Shipping> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Shipping.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.shippingRepository = (ShippingRepository)_jpaRepo;
    }

    @Override
    public Shipping FromModelToDto(pl.altemic.auctionhouse.models.Shipping model, Object... args) {
        if(model == null) return null;
        Shipping dto = new Shipping();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Shipping FromDtoToModel(Shipping dto, Object... args) {
        pl.altemic.auctionhouse.models.Shipping model = shippingRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Shipping();
        }

        dtoToModelMapper.Convert(dto, model);
        boolean passedAuction = false;
        if(args!=null
                && args.length > 0
                && Map.class.isAssignableFrom(args[0].getClass())){
            Map<String, Object> alreadyMapped = (Map<String, Object>)args[0];
            Object foundMapped = serviceUtils.getAlreadyMapped(alreadyMapped, Auction.class, "");
            if(foundMapped != null){
                model.setAuction((pl.altemic.auctionhouse.models.Auction) foundMapped);
                passedAuction = true;
            }
        }

        if(!passedAuction && dto.getAuction()!=null && !identityComparator.areTheSame(model.getAuction(), dto.getAuction())){
            pl.altemic.auctionhouse.models.Auction auction = auctionServiceImpl.FromDtoToModel(dto.getAuction());
            model.setAuction(auction);
        }

        if(dto.getPrice()!=null && !identityComparator.areTheSame(model.getPrice(), dto.getPrice())){
            model.setPrice(priceServiceImpl.FromDtoToModel(dto.getPrice()));
        }
        if(dto.getTitleList()!=null && !identityComparator.areTheSame(model.getTitleList(), dto.getTitleList())){
            List<pl.altemic.auctionhouse.models.Title> titleList = dto.getTitleList().stream().map(t -> titleServiceImpl.FromDtoToModel(t)).collect(Collectors.toList());
            model.setTitleList(titleList);
        }
        if(dto.getDescriptionList()!=null && !identityComparator.areTheSame(model.getDescriptionList(), dto.getDescriptionList())){
            List<pl.altemic.auctionhouse.models.Description> descriptionList = dto.getDescriptionList().stream().map(d -> descriptionServiceImpl.FromDtoToModel(d)).collect(Collectors.toList());
            model.setDescriptionList(descriptionList);
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Shipping dto = (Shipping)target;
        pl.altemic.auctionhouse.models.Shipping model = shippingRepository.findOne(dto.getId());
        if(model.getPrice()!=null){
            dto.setPrice(priceServiceImpl.FromModelToDto(model.getPrice()));
        }
        if(model.getAuction()!=null){
            Auction auction = auctionServiceImpl.FromModelToDto(model.getAuction());
            dto.setAuction(auction);
        }
        if(model.getTitleList()!=null){
            List<Title> titleList = model.getTitleList().stream().map(t -> titleServiceImpl.FromModelToDto(t)).collect(Collectors.toList());
            dto.setTitleList(titleList);
        }
        if(model.getDescriptionList()!=null){
            List<Description> descriptionList = model.getDescriptionList().stream().map(d -> descriptionServiceImpl.FromModelToDto(d)).collect(Collectors.toList());
            dto.setDescriptionList(descriptionList);
        }
        return dto;
    }
}

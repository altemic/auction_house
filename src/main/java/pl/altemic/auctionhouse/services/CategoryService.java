package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Category;

/**
 * Created by m.altenberg on 20.12.2016.
 */
public interface CategoryService extends BaseCRUDService<Category> {
    Category findByName(String name);
}

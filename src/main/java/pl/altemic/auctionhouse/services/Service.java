package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.guardian.GuardianConfig;

/**
 * Created by michal on 04.12.2016.
 */
public interface Service {
    String getEntityName();
}

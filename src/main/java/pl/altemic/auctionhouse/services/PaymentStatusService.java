package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.PaymentStatus;

/**
 * Created by michal on 15.11.2016.
 */
public interface PaymentStatusService extends BaseCRUDService<PaymentStatus> {
}

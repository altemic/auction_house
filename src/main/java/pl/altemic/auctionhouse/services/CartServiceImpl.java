package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Cart;
import pl.altemic.auctionhouse.dto.Offer;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.CartRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
public class CartServiceImpl extends BaseCRUDServiceImpl<Cart, pl.altemic.auctionhouse.models.Cart> implements CartService, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Cart, Cart> modelToDtoMapper;
    Mapper<Cart, pl.altemic.auctionhouse.models.Cart> dtoToModelMapper;
    OfferServiceImpl offerServiceImpl;
    CartRepository cartRepository;
    IdentityComparator identityComparator;
    UserServiceImpl userServiceImpl;

    @Autowired
    public void setOfferServiceImpl(OfferServiceImpl offerServiceImpl) {
        this.offerServiceImpl = offerServiceImpl;
    }

    @Autowired
    public void setModelToDtoMapper(Mapper<pl.altemic.auctionhouse.models.Cart, Cart> modelToDtoMapper) {
        this.modelToDtoMapper = modelToDtoMapper;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public void setUserServiceImpl(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @Autowired
    public CartServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Cart, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Cart> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Cart, Cart> _modelToDtoMapper,
            Mapper<Cart, pl.altemic.auctionhouse.models.Cart> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Cart.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.cartRepository = (CartRepository)_jpaRepo;
    }

    @Override
    public Cart FromModelToDto(pl.altemic.auctionhouse.models.Cart model, Object... args) {
        if(model == null) return null;
        Cart dto = new Cart();
        modelToDtoMapper.Convert(model, dto);
        if(model.getOfferList()!=null) {
            List<Offer> offerList = model.getOfferList().stream().map(o -> offerServiceImpl.FromModelToDto(o)).collect(Collectors.toList());
            dto.setOfferList(offerList);
        }
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Cart FromDtoToModel(Cart dto, Object... args) {
        pl.altemic.auctionhouse.models.Cart model = cartRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Cart();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getOfferList()!=null && !identityComparator.areTheSame(model.getOfferList(), dto.getOfferList())) {
            List<pl.altemic.auctionhouse.models.Offer> offerList = dto.getOfferList().stream().map(o -> offerServiceImpl.FromDtoToModel(o)).collect(Collectors.toList());
            model.setOfferList(offerList);
        }
        if(dto.getUser()!= null && !identityComparator.areTheSame(model.getUser(), dto.getUser())){
            model.setUser(userServiceImpl.FromDtoToModel(dto.getUser()));
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Cart dto = (Cart)target;
        pl.altemic.auctionhouse.models.Cart model = cartRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Cart();
        }
        modelToDtoMapper.Convert(model, dto);
        if(model.getUser()!= null){
            dto.setUser(userServiceImpl.FromModelToDto(model.getUser()));
        }
        if(model.getOfferList()!=null && model.getOfferList().size() > 0){
            List<Offer> offerList = model.getOfferList().stream().map(o -> offerServiceImpl.FromModelToDto(o)).collect(Collectors.toList());
            dto.setOfferList(offerList);
        }
        return dto;
    }
}

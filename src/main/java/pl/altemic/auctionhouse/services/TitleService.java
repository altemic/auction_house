package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Title;

import java.util.List;

/**
 * Created by michal on 16.11.2016.
 */
public interface TitleService extends BaseCRUDService<Title> {
    List<Title> findByText(String text);
    List<Title> findByOwner(long id);
}

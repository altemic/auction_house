package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Currency;

/**
 * Created by michal on 15.11.2016.
 */
public interface CurrencyService extends BaseCRUDService<Currency> {
}

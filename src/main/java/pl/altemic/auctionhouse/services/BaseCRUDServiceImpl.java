package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import pl.altemic.auctionhouse.guardian.GuardianConfig;
import pl.altemic.auctionhouse.guardian.GuardianControlled;
import pl.altemic.auctionhouse.guardian.PermissionType;

/**
 * Created by michal on 13.11.2016.
 */
//Annotating with TRANSACTIONAL lets oneToMany and ManyToMany collections to be loaded (Lazily - needed transaction)
@Transactional
public abstract class BaseCRUDServiceImpl<T,V> implements BaseCRUDService<T>, Converter<T,V>, GuardianControlled {

    protected BaseCRUDService<V> baseCRUDService;
    protected Class<T> dtoClass;

    @Autowired
    public BaseCRUDServiceImpl(BaseCRUDService<V> _baseCRUDService, Class<T> _dtoClass) {
        this.dtoClass = _dtoClass;
        this.baseCRUDService = _baseCRUDService;
    }

    @Override
    @PermissionType(permissionType = pl.altemic.auctionhouse.dto.PermissionType.ADD)
    public T save(T dto) {
        if(dto == null) return null;
        V model = FromDtoToModel(dto);
        model = baseCRUDService.save(model);
        dto = FromModelToDto(model);
        return dto;
    }

    @Override
    @PermissionType(permissionType = pl.altemic.auctionhouse.dto.PermissionType.UPDATE)
    public T update(T dto) {
        if(dto == null) return null;
        V model = FromDtoToModel(dto);
        V updated = baseCRUDService.update(model);
        return FromModelToDto(updated);
    }

    @Override
    @PermissionType(permissionType = pl.altemic.auctionhouse.dto.PermissionType.REMOVE)
    public void remove(Long id) {
        baseCRUDService.remove(id);
    }

    @Override
    @PermissionType(permissionType = pl.altemic.auctionhouse.dto.PermissionType.VIEW)
    public T findOne(Long id) {
        if(id <= 0) return null;
        V model = baseCRUDService.findOne(id);
        T dto = FromModelToDto(model);
        return dto;
    }

    @Override
    @GuardianConfig(NotControlled = true)
    public String getEntityName(){
        return dtoClass.getSimpleName();
    }

    @GuardianConfig(NotControlled = true)
    public abstract T FromModelToDto(V model, Object... args);

    @GuardianConfig(NotControlled = true)
    public abstract V FromDtoToModel(T dto, Object... args);
}
package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Price;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.PriceRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
//@DependsOn("currencyServiceImpl")
public class PriceServiceImpl extends BaseCRUDServiceImpl<Price, pl.altemic.auctionhouse.models.Price> implements PriceService, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Price, Price> modelToDtoMapper;
    Mapper<Price, pl.altemic.auctionhouse.models.Price> dtoToModelMapper;
    PriceRepository priceRepository;
    IdentityComparator identityComparator;
    CurrencyServiceImpl currencyServiceImpl;

    @Autowired
    public void setCurrencyServiceImpl(CurrencyServiceImpl currencyServiceImpl) {
        this.currencyServiceImpl = currencyServiceImpl;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public PriceServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Price, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Price> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Price, Price> _modelToDtoMapper,
            Mapper<Price, pl.altemic.auctionhouse.models.Price> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Price.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.priceRepository = (PriceRepository)_jpaRepo;
    }

    @Override
    public Price FromModelToDto(pl.altemic.auctionhouse.models.Price model, Object... args) {
        if(model == null) return null;
        Price dto = new Price();
        modelToDtoMapper.Convert(model, dto);
        if(model.getCurrency()!=null){
            dto.setCurrency(currencyServiceImpl.FromModelToDto(model.getCurrency()));
        }
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Price FromDtoToModel(Price dto, Object... args) {
        pl.altemic.auctionhouse.models.Price model = priceRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Price();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getCurrency()!=null && !identityComparator.areTheSame(model.getCurrency(), dto.getCurrency())){
            model.setCurrency(currencyServiceImpl.FromDtoToModel(dto.getCurrency()));
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Price dto = (Price)target;
        pl.altemic.auctionhouse.models.Price model = priceRepository.findOne(dto.getId());
        if(model == null) return null;
        modelToDtoMapper.Convert(model, dto);
        if(model.getCurrency()!=null){
            dto.setCurrency(currencyServiceImpl.FromModelToDto(model.getCurrency()));
        }
        return dto;
    }
}

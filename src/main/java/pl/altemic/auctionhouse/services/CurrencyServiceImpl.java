package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Currency;
import pl.altemic.auctionhouse.repositories.CurrencyRepository;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
public class CurrencyServiceImpl extends BaseCRUDServiceImpl<Currency, pl.altemic.auctionhouse.models.Currency> implements CurrencyService {

    Mapper<pl.altemic.auctionhouse.models.Currency, Currency> modelToDtoMapper;
    Mapper<Currency, pl.altemic.auctionhouse.models.Currency> dtoToModelMapper;
    CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Currency, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Currency> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Currency, Currency> _modelToDtoMapper,
            Mapper<Currency, pl.altemic.auctionhouse.models.Currency> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Currency.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.currencyRepository = (CurrencyRepository)_jpaRepo;
    }

    @Override
    public Currency FromModelToDto(pl.altemic.auctionhouse.models.Currency model, Object... args) {
        if(model == null) return null;
        Currency dto = new Currency();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Currency FromDtoToModel(Currency dto, Object... args) {
        pl.altemic.auctionhouse.models.Currency model = currencyRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Currency();
        }
        dtoToModelMapper.Convert(dto, model);
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }
}

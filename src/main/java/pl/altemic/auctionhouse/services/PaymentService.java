package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Payment;

/**
 * Created by michal on 15.11.2016.
 */
public interface PaymentService extends BaseCRUDService<Payment> {
}

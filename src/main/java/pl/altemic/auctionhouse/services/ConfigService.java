package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Config;

/**
 * Created by michal on 20.11.2016.
 */
public interface ConfigService extends BaseCRUDService<Config> {
    Config findByName(String name);
}

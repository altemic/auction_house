package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Cart;

/**
 * Created by michal on 15.11.2016.
 */
public interface CartService extends BaseCRUDService<Cart> {
}

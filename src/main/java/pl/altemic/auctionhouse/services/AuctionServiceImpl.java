package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Auction;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.AuctionRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
@DependsOn({"shippingServiceImpl", "descriptionServiceImpl", "titleServiceImpl"})
public class AuctionServiceImpl extends BaseCRUDServiceImpl<Auction, pl.altemic.auctionhouse.models.Auction> implements AuctionService, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Auction, Auction> modelToDtoMapper;
    Mapper<Auction, pl.altemic.auctionhouse.models.Auction> dtoToModelMapper;
    TitleServiceImpl titleServiceImpl;
    DescriptionServiceImpl descriptionServiceImpl;
    UserServiceImpl userServiceImpl;
    OfferServiceImpl offerServiceImpl;
    ShippingServiceImpl shippingServiceImpl;
    AuctionRepository auctionRepository;
    IdentityComparator identityuComparator;

    @Autowired
    public void setTitleServiceImpl(@Lazy  TitleServiceImpl titleServiceImpl) {
        this.titleServiceImpl = titleServiceImpl;
    }

    @Autowired
    public void setDescriptionServiceImpl(@Lazy DescriptionServiceImpl descriptionServiceImpl) {
        this.descriptionServiceImpl = descriptionServiceImpl;
    }

    @Autowired
    public void setUserServiceImpl(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

    @Autowired
    public void setOfferServiceImpl(@Lazy OfferServiceImpl offerServiceImpl) {
        this.offerServiceImpl = offerServiceImpl;
    }

    @Autowired
    public void setShippingServiceImpl(@Lazy ShippingServiceImpl shippingServiceImpl) {
        this.shippingServiceImpl = shippingServiceImpl;
    }

    @Autowired
    public void setIdentityuComparator(IdentityComparator identityuComparator) {
        this.identityuComparator = identityuComparator;
    }

    @Autowired
    public AuctionServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Auction, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Auction> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Auction, Auction> _modelToDtoMapper,
            Mapper<Auction, pl.altemic.auctionhouse.models.Auction> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Auction.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        auctionRepository = (AuctionRepository)_jpaRepo;
    }

    @Override
    public Auction FromModelToDto(pl.altemic.auctionhouse.models.Auction model, Object... args) {
        if(model == null) return null;
        Auction dto = new Auction();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Auction FromDtoToModel(Auction dto, Object... args) {
        pl.altemic.auctionhouse.models.Auction model = auctionRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Auction();
        }

        Map<String, Object> mapped = new HashMap();
        mapped.put("auction", model);

        dtoToModelMapper.Convert(dto, model);
        if(dto.getTitleList()!=null && !identityuComparator.areTheSame(dto.getTitleList(), model.getTitleList())){
            List<pl.altemic.auctionhouse.models.Title> titleList = dto.getTitleList().stream().map(t -> titleServiceImpl.FromDtoToModel(t, mapped)).collect(Collectors.toList());
            model.setTitleList(titleList);
        }
        if(dto.getDescriptionList()!=null && !identityuComparator.areTheSame(dto.getDescriptionList(), model.getDescriptionList())){
            List<pl.altemic.auctionhouse.models.Description> descriptionList = dto.getDescriptionList().stream().map(d -> descriptionServiceImpl.FromDtoToModel(d, mapped)).collect(Collectors.toList());
            model.setDescriptionList(descriptionList);
        }
        if(dto.getSeller()!=null && !identityuComparator.areTheSame(dto.getSeller(), model.getSeller())){
            model.setSeller(userServiceImpl.FromDtoToModel(dto.getSeller()));
        }
        if(dto.getOfferList()!=null && !identityuComparator.areTheSame(dto.getOfferList(), model.getOfferList())){
            List<pl.altemic.auctionhouse.models.Offer> offerList = dto.getOfferList().stream().map(o -> offerServiceImpl.FromDtoToModel(o, mapped)).collect(Collectors.toList());
            model.setOfferList(offerList);
        }
        if(dto.getShippingList()!=null && !identityuComparator.areTheSame(dto.getShippingList(), model.getShippingList())){
            List<pl.altemic.auctionhouse.models.Shipping> shippingList = dto.getShippingList().stream().map(s -> shippingServiceImpl.FromDtoToModel(s, mapped)).collect(Collectors.toList());
            model.setShippingList(shippingList);
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Auction dto = (Auction)target;
        pl.altemic.auctionhouse.models.Auction model = auctionRepository.findOne(dto.getId());
        if(model == null) return null;
        dtoToModelMapper.Convert(dto, model);
        if(model.getTitleList()!=null){
            List<pl.altemic.auctionhouse.dto.Title> titleList = model.getTitleList().stream().map(t -> titleServiceImpl.FromModelToDto(t)).collect(Collectors.toList());
            dto.setTitleList(titleList);
        }
        if(model.getDescriptionList()!=null){
            List<pl.altemic.auctionhouse.dto.Description> descriptionList = model.getDescriptionList().stream().map(d -> descriptionServiceImpl.FromModelToDto(d)).collect(Collectors.toList());
            dto.setDescriptionList(descriptionList);
        }
        if(model.getSeller()!=null){
            dto.setSeller(userServiceImpl.FromModelToDto(model.getSeller()));
        }
        if(model.getOfferList()!=null){
            List<pl.altemic.auctionhouse.dto.Offer> offerList = model.getOfferList().stream().map(o -> offerServiceImpl.FromModelToDto(o)).collect(Collectors.toList());
            dto.setOfferList(offerList);
        }
        if(model.getShippingList()!=null){
            List<pl.altemic.auctionhouse.dto.Shipping> shippingList = model.getShippingList().stream().map(s -> shippingServiceImpl.FromModelToDto(s)).collect(Collectors.toList());
            dto.setShippingList(shippingList);
        }
        return dto;
    }
}

package pl.altemic.auctionhouse.services;

/**
 * Created by michal on 14.11.2016.
 */
public interface BaseCRUDService<T> extends Service {
    T save(T obj);
    T update(T obj);
    void remove(Long id);
    T findOne(Long id);
}

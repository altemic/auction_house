package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Description;

/**
 * Created by michal on 15.11.2016.
 */
public interface DescriptionService extends BaseCRUDService<Description> {
}

package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Dictionary;
import pl.altemic.auctionhouse.repositories.DictionaryRepository;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by michal on 15.11.2016.
 */
@Repository
public class DictionaryServiceImpl extends BaseCRUDServiceImpl<Dictionary, pl.altemic.auctionhouse.models.Dictionary> implements DictionaryService {

    Mapper<pl.altemic.auctionhouse.models.Dictionary, Dictionary> modelToDtoMapper;
    Mapper<Dictionary, pl.altemic.auctionhouse.models.Dictionary> dtoToModelMapper;
    DictionaryRepository dictionaryRepository;

    @Autowired
    public DictionaryServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Dictionary, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Dictionary> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Dictionary, Dictionary> _modelToDtoMapper,
            Mapper<Dictionary, pl.altemic.auctionhouse.models.Dictionary> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Dictionary.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.dictionaryRepository = (DictionaryRepository)_jpaRepo;
    }

    @Override
    public Dictionary FromModelToDto(pl.altemic.auctionhouse.models.Dictionary model, Object... args) {
        if(model == null) return null;
        Dictionary dto = new Dictionary();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Dictionary FromDtoToModel(Dictionary dto, Object... args) {
        pl.altemic.auctionhouse.models.Dictionary model = dictionaryRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Dictionary();
        }
        dtoToModelMapper.Convert(dto, model);
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Dictionary findBySymbol(String symbol) {
        pl.altemic.auctionhouse.models.Dictionary model = dictionaryRepository.findBySymbol(symbol);
        if(model == null) return null;
        Dictionary dto = FromModelToDto(model);
        return dto;
    }
}

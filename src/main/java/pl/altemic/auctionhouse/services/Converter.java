package pl.altemic.auctionhouse.services;

/**
 * Created by m.altenberg on 23.12.2016.
 */
public interface Converter<T,V> {
    T FromModelToDto(V model, Object... args);
    V FromDtoToModel(T dto, Object... args);
}

package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Offer;

/**
 * Created by michal on 15.11.2016.
 */
public interface OfferService extends BaseCRUDService<Offer> {
}

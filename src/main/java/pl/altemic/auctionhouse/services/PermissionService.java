package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Permission;

/**
 * Created by michal on 13.11.2016.
 */
public interface PermissionService extends BaseCRUDService<Permission> {
    Permission findByNameAndReference(String name, String reference);
    Permission findByName(String name);
    public Permission findByTypeAndReference(String type, String reference);
}

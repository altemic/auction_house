package pl.altemic.auctionhouse.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * Created by michal on 14.11.2016.
 */

@Component
public class RepositoryWrapperImpl<T> implements RepositoryWrapper<T> {

    @Override
    public BaseCRUDService<T> JpaRepositoryWrapper(JpaRepository<T, Long> jpaRepository) {

        BaseCRUDService<T> wrapper = new BaseCRUDService<T>() {

            @Override
            public String getEntityName() {
                return null;
            }

            @Override
            public T save(T obj) {return jpaRepository.save(obj); }

            @Override
            public T update(T obj) { return jpaRepository.save(obj); }

            @Override
            public void remove(Long id) {
                jpaRepository.delete(id);
            }

            @Override
            public T findOne(Long id) {
                return jpaRepository.findOne(id);
            }
        };

        return wrapper;
    }
}

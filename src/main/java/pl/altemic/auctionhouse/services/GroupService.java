package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Group;

/**
 * Created by michal on 15.11.2016.
 */
public interface GroupService extends BaseCRUDService<Group> {
    Group findByName(String name);
}

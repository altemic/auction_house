package pl.altemic.auctionhouse.services;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.guardian.GuardianConfig;

import java.util.List;

/**
 * Created by michal on 03.12.2016.
 */

@Repository
public class ServiceFactoryImpl implements ServiceFactory {

    List<Service> serviceList;

    @Lazy
    @Autowired
    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    @Override
    @GuardianConfig(NotControlled = true)
    public <T> T getService(String entityName) throws ClassNotFoundException {

        for (Service s : serviceList) {
            String name = s.getEntityName();
            if (name.equals(entityName)) {
                T service = (T) s;
                return service;
            }
        }

        throw new ClassNotFoundException("Class for entityName: " + entityName + "NOT FOUND");
    }
}

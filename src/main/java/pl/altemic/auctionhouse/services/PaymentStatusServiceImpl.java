package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.PaymentStatus;
import pl.altemic.auctionhouse.repositories.PaymentStatusRepository;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
public class PaymentStatusServiceImpl extends BaseCRUDServiceImpl<PaymentStatus, pl.altemic.auctionhouse.models.PaymentStatus> implements PaymentStatusService {

    Mapper<pl.altemic.auctionhouse.models.PaymentStatus, PaymentStatus> modelToDtoMapper;
    Mapper<PaymentStatus, pl.altemic.auctionhouse.models.PaymentStatus> dtoToModelMapper;
    PaymentStatusRepository paymentStatusRepository;

    @Autowired
    public PaymentStatusServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.PaymentStatus, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.PaymentStatus> wrapper,
            Mapper<pl.altemic.auctionhouse.models.PaymentStatus, PaymentStatus> _modelToDtoMapper,
            Mapper<PaymentStatus, pl.altemic.auctionhouse.models.PaymentStatus> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), PaymentStatus.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.paymentStatusRepository = (PaymentStatusRepository)_jpaRepo;
    }

    @Override
    public PaymentStatus FromModelToDto(pl.altemic.auctionhouse.models.PaymentStatus model, Object... args) {
        if(model == null) return null;
        PaymentStatus dto = new PaymentStatus();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.PaymentStatus FromDtoToModel(PaymentStatus dto, Object... args) {
        pl.altemic.auctionhouse.models.PaymentStatus model = paymentStatusRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.PaymentStatus();
        }
        dtoToModelMapper.Convert(dto, model);
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }
}

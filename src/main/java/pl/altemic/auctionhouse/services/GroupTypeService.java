package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.GroupType;

/**
 * Created by michal on 15.11.2016.
 */
public interface GroupTypeService extends BaseCRUDService<GroupType> {
    GroupType findByName(String name);
}

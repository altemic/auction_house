package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.Shipping;

/**
 * Created by michal on 15.11.2016.
 */
public interface ShippingService extends BaseCRUDService<Shipping> {
}

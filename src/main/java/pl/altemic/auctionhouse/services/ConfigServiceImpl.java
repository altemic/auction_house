package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Config;
import pl.altemic.auctionhouse.repositories.ConfigRepository;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by michal on 20.11.2016.
 */

@Repository
public class ConfigServiceImpl extends BaseCRUDServiceImpl<Config, pl.altemic.auctionhouse.models.Config> implements ConfigService {

    Mapper<pl.altemic.auctionhouse.models.Config, Config> modelToDtoMapper;
    Mapper<Config, pl.altemic.auctionhouse.models.Config> dtoToModelMapper;
    ConfigRepository configRepository;

    @Autowired
    public ConfigServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Config, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Config> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Config, Config> _modelToDtoMapper,
            Mapper<Config, pl.altemic.auctionhouse.models.Config> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Config.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.configRepository = (ConfigRepository)_jpaRepo;
    }

    @Override
    public Config FromModelToDto(pl.altemic.auctionhouse.models.Config model, Object... args) {
        if(model == null) return null;
        Config dto = new Config();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Config FromDtoToModel(Config dto, Object... args) {
        pl.altemic.auctionhouse.models.Config model = null;
        if(dto.getId() != 0){
            model = configRepository.findOne(dto.getId());
        }
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Config();
        }
        dtoToModelMapper.Convert(dto, model);
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Config findByName(String name) {
        pl.altemic.auctionhouse.models.Config model = configRepository.findByName(name);
        if(model==null) return null;
        return FromModelToDto(model);
    }
}

package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Payment;
import pl.altemic.auctionhouse.dto.PaymentStatus;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.PaymentRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michal on 15.11.2016.
 */

@Repository
//@DependsOn("paymentStatusServiceImpl")
public class PaymentServiceImpl extends BaseCRUDServiceImpl<Payment, pl.altemic.auctionhouse.models.Payment> implements PaymentService, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Payment, Payment> modelToDtoMapper;
    Mapper<Payment, pl.altemic.auctionhouse.models.Payment> dtoToModelMapper;
    PaymentStatusServiceImpl paymentStatusServiceImpl;
    PaymentRepository paymentRepository;
    IdentityComparator identityComparator;

    @Autowired
    public void setPaymentStatusServiceImpl(PaymentStatusServiceImpl paymentStatusServiceImpl) {
        this.paymentStatusServiceImpl = paymentStatusServiceImpl;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public PaymentServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Payment, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Payment> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Payment, Payment> _modelToDtoMapper,
            Mapper<Payment, pl.altemic.auctionhouse.models.Payment> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Payment.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.paymentRepository = (PaymentRepository)_jpaRepo;
    }

    @Override
    public Payment FromModelToDto(pl.altemic.auctionhouse.models.Payment model, Object... args) {
        if(model == null) return null;
        Payment dto = new Payment();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Payment FromDtoToModel(Payment dto, Object... args) {
        pl.altemic.auctionhouse.models.Payment model = paymentRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Payment();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getPaymentStatuses()!=null && !identityComparator.areTheSame(model.getPaymentStatuses(), dto.getPaymentStatuses())){
            List<pl.altemic.auctionhouse.models.PaymentStatus> paymentList = dto.getPaymentStatuses().stream().map(p -> paymentStatusServiceImpl.FromDtoToModel(p)).collect(Collectors.toList());
            model.setPaymentStatuses(paymentList);
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Payment dto = (Payment)target;
        pl.altemic.auctionhouse.models.Payment model = paymentRepository.findOne(dto.getId());
        if(model == null) return null;
        modelToDtoMapper.Convert(model, dto);
        if(model.getPaymentStatuses()!=null){
            List<PaymentStatus> paymentStatusList = model.getPaymentStatuses().stream().map(p -> paymentStatusServiceImpl.FromModelToDto(p)).collect(Collectors.toList());
            dto.setPaymentStatuses(paymentStatusList);
        }
        return dto;
    }
}

package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Description;
import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.models.Owner;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.DescriptionRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;
import pl.altemic.auctionhouse.utils.OwnableConverter;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by michal on 15.11.2016.
 */
@Repository
public class DescriptionServiceImpl extends BaseCRUDServiceImpl<Description, pl.altemic.auctionhouse.models.Description> implements DescriptionService, Optimizer {

    DescriptionRepository descriptionRepository;
    AuctionServiceImpl auctionServiceImpl;
    Mapper<pl.altemic.auctionhouse.models.Description, Description> modelToDtoMapper;
    Mapper<Description, pl.altemic.auctionhouse.models.Description> dtoToModelMapper;
    ServiceFactory serviceFactory;
    DictionaryServiceImpl dictionaryServiceImpl;
    OwnableConverter ownableConverter;
    IdentityComparator identityComparator;
    ServiceUtils serviceUtils;

    @Autowired
    public void setAuctionServiceImpl(@Lazy AuctionServiceImpl auctionServiceImpl) {
        this.auctionServiceImpl = auctionServiceImpl;
    }

    @Autowired
    public void setServiceFactory(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Autowired
    public void setDictionaryServiceImpl(DictionaryServiceImpl dictionaryServiceImpl) {
        this.dictionaryServiceImpl = dictionaryServiceImpl;
    }

    @Autowired
    public void setOwnableConverter(OwnableConverter ownableConverter) {
        this.ownableConverter = ownableConverter;
    }

    @Autowired
    public void setDescriptionRepository(DescriptionRepository descriptionRepository) {
        this.descriptionRepository = descriptionRepository;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public void setServiceUtils(ServiceUtils serviceUtils) {
        this.serviceUtils = serviceUtils;
    }

    @Autowired
    public DescriptionServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Description, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Description> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Description, Description> _modelToDtoMapper,
            Mapper<Description, pl.altemic.auctionhouse.models.Description> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Description.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.descriptionRepository = (DescriptionRepository)_jpaRepo;
    }

    @Override
    public Description FromModelToDto(pl.altemic.auctionhouse.models.Description model, Object... args) {
        if(model == null) return null;
        Description dto = new Description();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Description FromDtoToModel(Description dto, Object... args) {
        pl.altemic.auctionhouse.models.Description model = descriptionRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Description();
        }
        dtoToModelMapper.Convert(dto, model);
        if(dto.getOwner()!=null && !identityComparator.areTheSame(model.getOwner(), dto.getOwner())){
            try {

                boolean assigned = false;
                if(args!=null
                        && args.length > 0
                        && Map.class.isAssignableFrom(args[0].getClass())){
                    Map<String, Object> alreadyMapped = (Map<String, Object>)args[0];
                    Object foundMapped = serviceUtils.getAlreadyMapped(alreadyMapped, Owner.class, "");
                    if(foundMapped != null){
                        Owner owner = (Owner)foundMapped;
                        model.setOwner(owner);
                        assigned = true;
                    }
                }

                if(!assigned) {
                    Owner owner = ownableConverter.FromDtoToModel(dto.getOwner());
                    model.setOwner(owner);
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if(dto.getDictionary()!=null && !identityComparator.areTheSame(model.getDictionary(), dto.getDictionary())){
            model.setDictionary(dictionaryServiceImpl.FromDtoToModel(dto.getDictionary()));
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Description dto = (Description)target;
        pl.altemic.auctionhouse.models.Description model = descriptionRepository.findOne(dto.getId());
        if(model.getOwner()!=null){
            try {
                Ownable owner = ownableConverter.FromModelToDto(model.getOwner());
                dto.setOwner(owner);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if(model.getDictionary()!=null){
            dto.setDictionary(dictionaryServiceImpl.FromModelToDto(model.getDictionary()));
        }
        return dto;
    }
}

package pl.altemic.auctionhouse.services;

import pl.altemic.auctionhouse.dto.CategoryType;

/**
 * Created by m.altenberg on 20.12.2016.
 */
public interface CategoryTypeService extends BaseCRUDService<CategoryType>{
    CategoryType findByName(String name);
}

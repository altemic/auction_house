package pl.altemic.auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.altemic.auctionhouse.dto.Category;
import pl.altemic.auctionhouse.dto.CategoryType;
import pl.altemic.auctionhouse.dto.Description;
import pl.altemic.auctionhouse.dto.Title;
import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.optimizer.Optimizer;
import pl.altemic.auctionhouse.repositories.CategoryRepository;
import pl.altemic.auctionhouse.utils.IdentityComparator;
import pl.altemic.auctionhouse.utils.Mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by m.altenberg on 20.12.2016.
 */

@Repository
public class CategoryServiceImpl extends BaseCRUDServiceImpl<Category, pl.altemic.auctionhouse.models.Category> implements CategoryService, Optimizer {

    Mapper<pl.altemic.auctionhouse.models.Category, Category> modelToDtoMapper;
    Mapper<Category, pl.altemic.auctionhouse.models.Category> dtoToModelMapper;
    TitleServiceImpl titleServiceImpl;
    DescriptionServiceImpl descriptionServiceImpl;
    CategoryTypeServiceImpl categoryTypeServiceImpl;
    CategoryRepository categoryRepository;
    IdentityComparator identityComparator;

    @Autowired
    public void setCategoryTypeServiceImpl(CategoryTypeServiceImpl categoryTypeServiceImpl) {
        this.categoryTypeServiceImpl = categoryTypeServiceImpl;
    }

    @Autowired
    public void setTitleServiceImpl(TitleServiceImpl titleServiceImpl) {
        this.titleServiceImpl = titleServiceImpl;
    }

    @Autowired
    public void setDescriptionServiceImpl(DescriptionServiceImpl descriptionServiceImpl) {
        this.descriptionServiceImpl = descriptionServiceImpl;
    }

    @Autowired
    public void setIdentityComparator(IdentityComparator identityComparator) {
        this.identityComparator = identityComparator;
    }

    @Autowired
    public CategoryServiceImpl(
            JpaRepository<pl.altemic.auctionhouse.models.Category, Long> _jpaRepo,
            RepositoryWrapper<pl.altemic.auctionhouse.models.Category> wrapper,
            Mapper<pl.altemic.auctionhouse.models.Category, Category> _modelToDtoMapper,
            Mapper<Category, pl.altemic.auctionhouse.models.Category> _dtoToModelMapper) {
        super(wrapper.JpaRepositoryWrapper(_jpaRepo), Category.class);
        this.modelToDtoMapper = _modelToDtoMapper;
        this.dtoToModelMapper = _dtoToModelMapper;
        this.categoryRepository = (CategoryRepository)_jpaRepo;
    }

    @Override
    public Category FromModelToDto(pl.altemic.auctionhouse.models.Category model, Object... args) {
        if(model == null) return null;
        Category dto = new Category();
        modelToDtoMapper.Convert(model, dto);
        return dto;
    }

    @Override
    public pl.altemic.auctionhouse.models.Category FromDtoToModel(Category dto, Object... args) {
        pl.altemic.auctionhouse.models.Category model = categoryRepository.findOne(dto.getId());
        if(model == null){
            model = new pl.altemic.auctionhouse.models.Category();
        }
        long id = model.getId();
        dtoToModelMapper.Convert(dto, model);
        if(dto.getCategoryTypeList()!=null && !identityComparator.areTheSame(model.getCategoryTypeList(), dto.getCategoryTypeList())){
            List<pl.altemic.auctionhouse.models.CategoryType> categoryTypeList = dto.getCategoryTypeList().stream().map(c -> categoryTypeServiceImpl.FromDtoToModel(c)).collect(Collectors.toList());
            model.setCategoryTypeList(categoryTypeList);
        }
        if(dto.getDependingCategoryList()!=null && !identityComparator.areTheSame(model.getDependingCategoryList(), dto.getDependingCategoryList())){
            List<Category> dependingCategoryList = dto.getDependingCategoryList().stream().filter(p -> p.getId()!=id).collect(Collectors.toList());
            List<pl.altemic.auctionhouse.models.Category> dependingCategoryListDto = dependingCategoryList.stream().map(c -> FromDtoToModel(c)).collect(Collectors.toList());
            model.setDependingCategoryList(dependingCategoryListDto);
        }
        if(dto.getDescriptionList()!=null && !identityComparator.areTheSame(model.getDescriptionList(), dto.getDescriptionList())){
            List<pl.altemic.auctionhouse.models.Description> descriptionList = dto.getDescriptionList().stream().map(d -> descriptionServiceImpl.FromDtoToModel(d)).collect(Collectors.toList());
            model.setDescriptionList(descriptionList);
        }
        if(dto.getTitleList()!=null && !identityComparator.areTheSame(model.getTitleList(), dto.getTitleList())){
            List<pl.altemic.auctionhouse.models.Title> titleList = dto.getTitleList().stream().map(t -> titleServiceImpl.FromDtoToModel(t)).collect(Collectors.toList());
            model.setTitleList(titleList);
        }
        model.setTimeModified(new Timestamp(Calendar.getInstance().getTime().getTime()));
        if(model.getTimeCreated() == null){
            model.setTimeCreated(new Timestamp(Calendar.getInstance().getTime().getTime()));
        }
        return model;
    }

    @Override
    public Category findByName(String name) {
        pl.altemic.auctionhouse.models.Category categoryModel = categoryRepository.findByName(name);
        if(categoryModel == null) return null;
        Category category = FromModelToDto(categoryModel);
        return category;
    }

    @Override
    public Object LoadAllFields(Object target) {
        Category dto = (Category)target;
        pl.altemic.auctionhouse.models.Category model = categoryRepository.findOne(dto.getId());
        if(model == null) return dto;
        modelToDtoMapper.Convert(model, dto);
        if(model.getCategoryTypeList()!=null && model.getCategoryTypeList().size() > 0){
            List<CategoryType> categoryTypeList = model.getCategoryTypeList().stream().map(c -> categoryTypeServiceImpl.FromModelToDto(c)).collect(Collectors.toList());
            dto.setCategoryTypeList(categoryTypeList);
        }
        if(model.getDependingCategoryList()!=null && model.getDependingCategoryList().size() > 0){
            List<Category> categoryList = model.getDependingCategoryList().stream().map(c -> FromModelToDto(c)).collect(Collectors.toList());
            dto.setDependingCategoryList(categoryList);
        }
        if(model.getTitleList()!= null && model.getTitleList().size() > 0){
            List<Title> titleList = model.getTitleList().stream().map(t -> titleServiceImpl.FromModelToDto(t)).collect(Collectors.toList());
            dto.setTitleList(titleList);
        }
        if(model.getDescriptionList()!=null && model.getDescriptionList().size() > 0){
            List<Description> descriptionList = model.getDescriptionList().stream().map(d -> descriptionServiceImpl.FromModelToDto(d)).collect(Collectors.toList());
            dto.setDescriptionList(descriptionList);
        }
        return dto;
    }
}

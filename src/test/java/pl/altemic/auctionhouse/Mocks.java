package pl.altemic.auctionhouse;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.altemic.auctionhouse.dto.User;
import pl.altemic.auctionhouse.security.ContextUser;
import pl.altemic.auctionhouse.security.ContextUserImpl;
import pl.altemic.auctionhouse.security.SecurityUserServiceImpl;

/**
 * Created by m.altenberg on 11.02.2017.
 */

@Component
@Profile("test")
public class Mocks {

    @Bean
    @Primary
    public ContextUserImpl contextUser(){
        return Mockito.mock(ContextUserImpl.class);
    }

}

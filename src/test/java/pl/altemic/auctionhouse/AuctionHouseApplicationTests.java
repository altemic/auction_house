package pl.altemic.auctionhouse;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.altemic.auctionhouse.dto.*;
import pl.altemic.auctionhouse.guardian.Ownable;
import pl.altemic.auctionhouse.models.Owner;
import pl.altemic.auctionhouse.modules.*;
import pl.altemic.auctionhouse.security.ContextUser;
import pl.altemic.auctionhouse.utils.AuctionHouseException;
import pl.altemic.auctionhouse.utils.IdentityComparator;
//import org.springframework.test.context;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
//@ContextConfiguration(loader=AnnotationConfigContextLoader.class)
public class AuctionHouseApplicationTests {

    @Autowired
    ContextUser contextUser;

    @Autowired
	UserModule userModule;

	@Autowired
	TitleModule titleModule;

	@Autowired
	AuctionModule auctionModule;

	@Autowired
	DescriptionModule descriptionModule;

	@Autowired
	DictionaryModule dictionaryModule;

	@Autowired
	IdentityComparator identityComparator;

	@Test
	public void contextLoads() {

	}

	@Test
	public void addAuction() throws AuctionHouseException {
	    setLoggedInUserByUsername("admin");

		Dictionary dictionary = dictionaryModule.findBySymbol("PL");

        Auction auction = new Auction();
        auction.setSeller(contextUser.getLoggedInUser());
        auction = auctionModule.add(auction);
        Assert.assertNotEquals(0, auction.getId());

		Title title = new Title();
		title.setText("test title");
		title.setOwner(auction);
		title.setDictionary(dictionary);
		title = titleModule.add(title);
        Assert.assertNotEquals(0, title.getId());

		Description description = new Description();
		description.setText("description");
		description.setDictionary(dictionary);
		description.setOwner(auction);
		description = descriptionModule.add(description);
		Assert.assertNotEquals(0, description.getId());

        auction.setTitleList(new ArrayList<>());
        auction.getTitleList().add(title);
        auction.setDescriptionList(new ArrayList<>());
        auction.getDescriptionList().add(description);
        auction = auctionModule.update(auction);
        Assert.assertNotEquals(0, auction.getTitleList().size());

        Auction testAuction = auctionModule.findOne(auction.getId());
        List<Title> testAuctionTitleList = testAuction.getTitleList();
        Assert.assertEquals(1, testAuctionTitleList.size());
        Assert.assertEquals("test title", testAuctionTitleList.get(0).getText());

        List<Description> testAuctionDescriptionList = testAuction.getDescriptionList();
	}

	public void setLoggedInUserByUsername(String username){
        User dto = userModule.findByUsername(username);
        Mockito.when(contextUser.getLoggedInUser()).thenReturn(dto);
    }
}
